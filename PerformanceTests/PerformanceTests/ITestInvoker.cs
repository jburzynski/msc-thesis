﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    interface ITestInvoker
    {
        void Invoke(int entitiesCount, int attributesCount, OperationType operationType, AccessMethod accessMethod);
    }
}
