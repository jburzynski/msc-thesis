﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    static class RandomUtils
    {
        public static Random Random { get; set; }
        public static readonly int MinStringLength = 5;
        public static readonly int MaxStringLength = 30;

        static RandomUtils()
        {
            Random = new Random();
        }

        public static string GenerateString(int length)
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[Random.Next(s.Length)])
                          .ToArray());
        }

        public static string GenerateString()
        {
            return GenerateString(MinStringLength, MaxStringLength);
        }

        public static string GenerateString(int minLength, int maxLength)
        {
            int length = Random.Next(maxLength - minLength) + minLength;
            return GenerateString(length);
        }

        public static int GenerateInt()
        {
            return Random.Next();
        }
    }
}
