﻿using MongoDB.Bson;
using MongoDB.Driver;
using PerformanceTests.Entities;
using Subdivision.Core;
using Subdivision.MongoDb;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    class MongoDbTestInvoker : ITestInvoker
    {
        private Stopwatch stopwatch;
        private MongoClient mongoClient;
        private IMongoCollection<TestEntity5> mongoCollection5;
        private IMongoCollection<TestEntity15> mongoCollection15;
        private IMongoCollection<TestEntity30> mongoCollection30;
        private IMongoCollection<BsonDocument> mongoCollectionBson;
        private MongoDbConnectionInfo subdivisionConnectionInfo;
        private TestSubdivisionContext subdivisionContext;
        private TestPubCompContext pubCompContext;

        private const string Collection5 = "test.benchmark.test5";
        private const string Collection15 = "test.benchmark.test15";
        private const string Collection30 = "test.benchmark.test30";
        private const string CollectionBson = "test.benchmark.bson";

        private const string WebApiUrl5 = "http://localhost:8080/test/" + Collection5;
        private const string WebApiUrl15 = "http://localhost:8080/test/" + Collection15;
        private const string WebApiUrl30 = "http://localhost:8080/test/" + Collection30;

        public MongoDbTestInvoker(Stopwatch stopwatch)
        {
            this.stopwatch = stopwatch;
            mongoClient = new MongoClient("mongodb://localhost:27017/test");
            IMongoDatabase mongoDatabase = mongoClient.GetDatabase("test");
            mongoCollection5 = mongoDatabase.GetCollection<TestEntity5>(Collection5);
            mongoCollection15 = mongoDatabase.GetCollection<TestEntity15>(Collection15);
            mongoCollection30 = mongoDatabase.GetCollection<TestEntity30>(Collection30);
            mongoCollectionBson = mongoDatabase.GetCollection<BsonDocument>(CollectionBson);
            subdivisionConnectionInfo = new MongoDbConnectionInfo
                {
                    Database = "test",
                    Host = "localhost",
                    Port = 27017
                };
            subdivisionContext = new TestSubdivisionContext(subdivisionConnectionInfo);
            pubCompContext = new TestPubCompContext("mongodb://localhost:27017/test", "test");
        }

        public void Invoke(int entitiesCount, int attributesCount, OperationType operationType, AccessMethod accessMethod)
        {
            switch (operationType)
            {
                case OperationType.Create:
                    InvokeCreate(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Read:
                    InvokeRead(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Update:
                    InvokeUpdate(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Delete:
                    InvokeDelete(entitiesCount, attributesCount, accessMethod);
                    break;
            }
        }

        private void InvokeCreate(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.PubComp:
                    InvokePubCompCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiCreate(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeRead(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            InvokeCreate(entitiesCount, attributesCount, accessMethod);

            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.PubComp:
                    InvokePubCompRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiRead(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeUpdate(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            InvokeCreate(entitiesCount, attributesCount, accessMethod);

            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.PubComp:
                    InvokePubCompUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiUpdate(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeDelete(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            InvokeCreate(entitiesCount, attributesCount, accessMethod);

            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.PubComp:
                    InvokePubCompDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiDelete(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeWebApiDelete(int entitiesCount, int attributesCount)
        {
            string baseUrl;
            IList<string> ids;
            switch (attributesCount)
            {
                default:
                case 5:
                    var entities5 = FixturesGenerator.CreateTestEntities5(entitiesCount);
                    mongoCollection5.InsertManyAsync(entities5).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities5.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl5;
                    break;
                case 15:
                    var entities15 = FixturesGenerator.CreateTestEntities15(entitiesCount);
                    mongoCollection15.InsertManyAsync(entities15).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities15.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl15;
                    break;
                case 30:
                    var entities30 = FixturesGenerator.CreateTestEntities30(entitiesCount);
                    mongoCollection30.InsertManyAsync(entities30).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities30.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl30;
                    break;
            }

            stopwatch.Reset();
            stopwatch.Start();
            foreach (string id in ids)
            {
                var request = (HttpWebRequest)WebRequest.Create(baseUrl + "/" + id);
                request.Method = "DELETE";
                request.GetResponse();
            }
            stopwatch.Stop();
        }

        private void InvokePubCompDelete(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokePubCompDelete5(entitiesCount);
                    break;
                case 15:
                    InvokePubCompDelete15(entitiesCount);
                    break;
                case 30:
                    InvokePubCompDelete30(entitiesCount);
                    break;
            }
        }

        private void InvokePubCompDelete5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity5> entitySet = pubCompContext.GetEntitySet<string, TestEntity5>(Collection5);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Delete(entities);
            stopwatch.Stop();
        }

        private void InvokePubCompDelete15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity15> entitySet = pubCompContext.GetEntitySet<string, TestEntity15>(Collection15);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Delete(entities);
            stopwatch.Stop();
        }

        private void InvokePubCompDelete30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity30> entitySet = pubCompContext.GetEntitySet<string, TestEntity30>(Collection30);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Delete(entities);
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete(int entitiesCount, int attributesCount)
        {
            subdivisionContext = new TestSubdivisionContext(subdivisionConnectionInfo);
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionDelete5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionDelete15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionDelete30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionDelete5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities5.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities15.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities30.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectDelete5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectDelete15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectDelete30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectDelete5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection5.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection15.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection30.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();
        }

        private void InvokeVendorDocumentDelete(int entitiesCount, int attributesCount)
        {
            IList<BsonDocument> documents = FixturesGenerator.CreateBsonDocuments(entitiesCount, attributesCount).ToList();
            mongoCollectionBson.InsertManyAsync(documents).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollectionBson.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();
        }

        private void InvokeWebApiUpdate(int entitiesCount, int attributesCount)
        {
            string baseUrl;
            IList<string> ids;
            switch (attributesCount)
            {
                default:
                case 5:
                    var entities5 = FixturesGenerator.CreateTestEntities5(entitiesCount);
                    mongoCollection5.InsertManyAsync(entities5).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities5.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl5;
                    break;
                case 15:
                    var entities15 = FixturesGenerator.CreateTestEntities15(entitiesCount);
                    mongoCollection15.InsertManyAsync(entities15).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities15.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl15;
                    break;
                case 30:
                    var entities30 = FixturesGenerator.CreateTestEntities30(entitiesCount);
                    mongoCollection30.InsertManyAsync(entities30).ConfigureAwait(false).GetAwaiter().GetResult();
                    ids = entities30.Select(e => e.Id).ToList();
                    baseUrl = WebApiUrl30;
                    break;
            }

            IList<string> documents = FixturesGenerator.CreateMongoJsonDocuments(entitiesCount, attributesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i++)
            {
                var request = (HttpWebRequest)WebRequest.Create(baseUrl + "/" + ids[i]);
                var data = Encoding.ASCII.GetBytes(documents[i]);
                request.Method = "PUT";
                request.ContentType = "application/hal+json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                request.GetResponse();
            }
            stopwatch.Stop();

            ClearCollection5();
            ClearCollection15();
            ClearCollection30();
        }

        private void InvokePubCompUpdate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokePubCompUpdate5(entitiesCount);
                    break;
                case 15:
                    InvokePubCompUpdate15(entitiesCount);
                    break;
                case 30:
                    InvokePubCompUpdate30(entitiesCount);
                    break;
            }
        }

        private void InvokePubCompUpdate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity5> entitySet = pubCompContext.GetEntitySet<string, TestEntity5>(Collection5);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Update(entities);
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokePubCompUpdate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity15> entitySet = pubCompContext.GetEntitySet<string, TestEntity15>(Collection15);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Update(entities);
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokePubCompUpdate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity30> entitySet = pubCompContext.GetEntitySet<string, TestEntity30>(Collection30);

            stopwatch.Reset();
            stopwatch.Start();
            entitySet.Update(entities);
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeSubdivisionUpdate(int entitiesCount, int attributesCount)
        {
            subdivisionContext = new TestSubdivisionContext(subdivisionConnectionInfo);
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionUpdate5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionUpdate15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionUpdate30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionUpdate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeSubdivisionUpdate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeSubdivisionUpdate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorObjectUpdate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectUpdate5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectUpdate15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectUpdate30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectUpdate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i++)
            {
                mongoCollection5.ReplaceOneAsync(new BsonDocument("_id", i.ToString()), entities[i]).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeVendorObjectUpdate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i++)
            {
                mongoCollection15.ReplaceOneAsync(new BsonDocument("_id", i.ToString()), entities[i]).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeVendorObjectUpdate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i++)
            {
                mongoCollection30.ReplaceOneAsync(new BsonDocument("_id", i.ToString()), entities[i]).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorDocumentUpdate(int entitiesCount, int attributesCount)
        {
            IList<BsonDocument> documents = FixturesGenerator.CreateBsonDocuments(entitiesCount, attributesCount).ToList();
            mongoCollectionBson.InsertManyAsync(documents).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i++)
            {
                mongoCollectionBson.ReplaceOneAsync(new BsonDocument("_id", i.ToString()), documents[i]).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            stopwatch.Stop();

            ClearCollectionBson();
        }

        private void InvokeWebApiRead(int entitiesCount, int attributesCount)
        {
            string url;
            switch (attributesCount)
            {
                default:
                case 5:
                    var entities5 = FixturesGenerator.CreateTestEntities5(entitiesCount);
                    mongoCollection5.InsertManyAsync(entities5).ConfigureAwait(false).GetAwaiter().GetResult();
                    url = WebApiUrl5;
                    break;
                case 15:
                    var entities15 = FixturesGenerator.CreateTestEntities15(entitiesCount);
                    mongoCollection15.InsertManyAsync(entities15).ConfigureAwait(false).GetAwaiter().GetResult();
                    url = WebApiUrl15;
                    break;
                case 30:
                    var entities30 = FixturesGenerator.CreateTestEntities30(entitiesCount);
                    mongoCollection30.InsertManyAsync(entities30).ConfigureAwait(false).GetAwaiter().GetResult();
                    url = WebApiUrl30;
                    break;
            }

            const int pageSize = 1000;
            string urlBase = url + "?pagesize=" + pageSize;

            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < entitiesCount; i += pageSize)
            {
                int page = i / pageSize + 1;
                var request = (HttpWebRequest)WebRequest.Create(urlBase + "&page=" + page);
                request.Method = "GET";
                request.GetResponse();
            }
            stopwatch.Stop();

            ClearCollection5();
            ClearCollection15();
            ClearCollection30();
        }

        private void InvokePubCompRead(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokePubCompRead5(entitiesCount);
                    break;
                case 15:
                    InvokePubCompRead15(entitiesCount);
                    break;
                case 30:
                    InvokePubCompRead30(entitiesCount);
                    break;
            }
        }

        private void InvokePubCompRead5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity5> entitySet = pubCompContext.GetEntitySet<string, TestEntity5>(Collection5);

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entitySet.Get(keys))
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokePubCompRead15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity15> entitySet = pubCompContext.GetEntitySet<string, TestEntity15>(Collection15);

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entitySet.Get(keys))
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokePubCompRead30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            IList<string> keys = entities.Select(e => e.Id).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            PubComp.NoSql.Core.IEntitySet<string, TestEntity30> entitySet = pubCompContext.GetEntitySet<string, TestEntity30>(Collection30);

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entitySet.Get(keys))
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeSubdivisionRead(int entitiesCount, int attributesCount)
        {
            subdivisionContext = new TestSubdivisionContext(subdivisionConnectionInfo);
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionRead5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionRead15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionRead30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionRead5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities5.GetAll())
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeSubdivisionRead15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities15.GetAll())
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeSubdivisionRead30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities30.GetAll())
            {
                // read
            }
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorObjectRead(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectRead5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectRead15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectRead30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectRead5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            using (var cursor = mongoCollection5.Find(new BsonDocument()).ToCursorAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            {
                while (cursor.MoveNextAsync().ConfigureAwait(false).GetAwaiter().GetResult())
                {
                    foreach (var entity in cursor.Current)
                    {
                        // iteration
                    }
                }
            }
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeVendorObjectRead15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            using (var cursor = mongoCollection15.Find(new BsonDocument()).ToCursorAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            {
                while (cursor.MoveNextAsync().ConfigureAwait(false).GetAwaiter().GetResult())
                {
                    foreach (var entity in cursor.Current)
                    {
                        // iteration
                    }
                }
            }
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeVendorObjectRead30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            using (var cursor = mongoCollection30.Find(new BsonDocument()).ToCursorAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            {
                while (cursor.MoveNextAsync().ConfigureAwait(false).GetAwaiter().GetResult())
                {
                    foreach (var entity in cursor.Current)
                    {
                        // iteration
                    }
                }
            }
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorDocumentRead(int entitiesCount, int attributesCount)
        {
            IList<BsonDocument> documents = FixturesGenerator.CreateBsonDocuments(entitiesCount, attributesCount).ToList();
            mongoCollectionBson.InsertManyAsync(documents).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            using (var cursor = mongoCollectionBson.Find(new BsonDocument()).ToCursorAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            {
                while (cursor.MoveNextAsync().ConfigureAwait(false).GetAwaiter().GetResult())
                {
                    foreach (var doc in cursor.Current)
                    {
                        // iteration
                    }
                }
            }
            stopwatch.Stop();

            ClearCollectionBson();
        }

        private void InvokeWebApiCreate(int entitiesCount, int attributesCount)
        {
            string url;
            switch (attributesCount)
            {
                default:
                case 5:
                    url = WebApiUrl5;
                    break;
                case 15:
                    url = WebApiUrl15;
                    break;
                case 30:
                    url = WebApiUrl30;
                    break;
            }

            IList<string> documents = FixturesGenerator.CreateMongoJsonDocuments(entitiesCount, attributesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (string document in documents)
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                var data = Encoding.ASCII.GetBytes(document);
                request.Method = "POST";
                request.ContentType = "application/hal+json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                request.GetResponse();
            }
            stopwatch.Stop();

            ClearCollection5();
            ClearCollection15();
            ClearCollection30();
        }

        private void InvokePubCompCreate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokePubCompCreate5(entitiesCount);
                    break;
                case 15:
                    InvokePubCompCreate15(entitiesCount);
                    break;
                case 30:
                    InvokePubCompCreate30(entitiesCount);
                    break;
            }
        }

        private void InvokePubCompCreate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            pubCompContext.GetEntitySet<string, TestEntity5>(Collection5).Add(entities);
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokePubCompCreate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            pubCompContext.GetEntitySet<string, TestEntity15>(Collection15).Add(entities);
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokePubCompCreate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            pubCompContext.GetEntitySet<string, TestEntity30>(Collection30).Add(entities);
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeSubdivisionCreate(int entitiesCount, int attributesCount)
        {
            subdivisionContext = new TestSubdivisionContext(subdivisionConnectionInfo);
            switch(attributesCount)
            {
                case 5:
                    InvokeSubdivisionCreate5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionCreate15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionCreate30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionCreate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeSubdivisionCreate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeSubdivisionCreate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorObjectCreate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectCreate5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectCreate15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectCreate30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectCreate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection5.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearCollection5();
        }

        private void InvokeVendorObjectCreate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection15.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearCollection15();
        }

        private void InvokeVendorObjectCreate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollection30.InsertManyAsync(entities).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearCollection30();
        }

        private void InvokeVendorDocumentCreate(int entitiesCount, int attributesCount)
        {
            IList<BsonDocument> documents = FixturesGenerator.CreateBsonDocuments(entitiesCount, attributesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            mongoCollectionBson.InsertManyAsync(documents).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearCollectionBson();
        }

        private void ClearCollectionBson()
        {
            mongoCollectionBson.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private void ClearCollection5()
        {
            mongoCollection5.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private void ClearCollection15()
        {
            mongoCollection15.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private void ClearCollection30()
        {
            mongoCollection30.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private class TestSubdivisionContext : MongoDbContext
        {
            public TestSubdivisionContext(MongoDbConnectionInfo connectionInfo) : base(connectionInfo)
            {
            }

            public IEntitySet<TestEntity5> TestEntities5 { get; set; }
            public IEntitySet<TestEntity15> TestEntities15 { get; set; }
            public IEntitySet<TestEntity30> TestEntities30 { get; set; }
        }

        private class TestPubCompContext : PubComp.NoSql.MongoDbDriver.MongoDbContext
        {
            public TestPubCompContext(string connectionString, string dbName) : base(connectionString, dbName)
            {
            }

            public PubComp.NoSql.Core.IEntitySet<string, TestEntity5> TestEntities5 { get; set; }
            public PubComp.NoSql.Core.IEntitySet<string, TestEntity15> TestEntities15 { get; set; }
            public PubComp.NoSql.Core.IEntitySet<string, TestEntity30> TestEntities30 { get; set; }
        }
    }
}
