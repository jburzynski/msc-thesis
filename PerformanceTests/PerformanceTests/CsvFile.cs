﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    class CsvFile
    {
        /**
         * Row format:
         * 
         * database_engine,access_method,operation_type,entities_count,attributes_count,mean_execution_time,mean_memory_usage<CR><LF>
         */
        private IList<IList<string>> values;
        private string fileName;
        private char separator;

        public CsvFile(string fileName, char separator = ';')
        {
            this.fileName = fileName;
            this.separator = separator;
            values = new List<IList<string>>();

            if (File.Exists(fileName))
            {
                foreach (string line in File.ReadLines(fileName))
                {
                    values.Add(line.Split(separator).ToList());
                }
            }
        }

        public void AddResult(string databaseEngine, string accessMethod, string operationType, int entitiesCount, int attributesCount, long executionTime, long memoryUsage)
        {
            values.Add(new List<string> {
                databaseEngine,
                accessMethod,
                operationType,
                entitiesCount.ToString(),
                attributesCount.ToString(),
                executionTime.ToString(),
                memoryUsage.ToString()
            });
        }

        public void Save()
        {
            File.WriteAllLines(fileName, values.Select(l => string.Join(separator.ToString(), l.ToArray())));
        }
    }
}
