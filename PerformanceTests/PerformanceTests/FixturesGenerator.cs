﻿using Amazon.DynamoDBv2.DocumentModel;
using MongoDB.Bson;
using PerformanceTests.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    static class FixturesGenerator
    {
        public static IEnumerable<BsonDocument> CreateBsonDocuments(int entitiesCount, int attributesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateBsonDocument(i.ToString(), attributesCount);
            }
        }

        public static IEnumerable<Document> CreateDynamoDocuments(int entitiesCount, int attributesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateDynamoDocument(i.ToString(), attributesCount);
            }
        }

        public static IEnumerable<string> CreateMongoJsonDocuments(int entitiesCount, int attributesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateMongoJsonDocument(i.ToString(), attributesCount);
            }
        }

        public static IEnumerable<TestEntity5> CreateTestEntities5(int entitiesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateTestEntity5(i.ToString());
            }
        }

        public static IEnumerable<TestEntity15> CreateTestEntities15(int entitiesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateTestEntity15(i.ToString());
            }
        }

        public static IEnumerable<TestEntity30> CreateTestEntities30(int entitiesCount)
        {
            for (int i = 0; i < entitiesCount; i++)
            {
                yield return CreateTestEntity30(i.ToString());
            }
        }

        private static BsonDocument CreateBsonDocument(string id, int attributesCount)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("_id", id);

            for (int i = 1; i < attributesCount; i++)
            {
                string key = "attr" + i;
                object value;
                if (i % 2 == 0)
                {
                    value = RandomUtils.GenerateInt();
                }
                else
                {
                    value = RandomUtils.GenerateString();
                }

                dictionary.Add(key, value);
            }

            return new BsonDocument(dictionary);
        }

        private static Document CreateDynamoDocument(string id, int attributesCount)
        {
            var dictionary = new Dictionary<string, DynamoDBEntry> { { "id", id } };

            for (int i = 1; i < attributesCount; i++)
            {
                string key = "attr" + i;
                if (i % 2 == 0)
                {
                    dictionary.Add(key, RandomUtils.GenerateInt());
                }
                else
                {
                    dictionary.Add(key, RandomUtils.GenerateString());
                }
            }

            return new Document(dictionary);
        }

        private static string CreateMongoJsonDocument(string id, int attributesCount)
        {
            string result = "{";
            result += "\"_id\": \"" + id + "\", ";

            for (int i = 1; i < attributesCount; i++)
            {
                string key = "attr" + i;
                object value;
                if (i % 2 == 0)
                {
                    value = RandomUtils.GenerateInt();
                }
                else
                {
                    value = RandomUtils.GenerateString();
                }

                result += "\"" + key + "\": \"" + value + "\", ";
            }

            return result + "}";
        }

        private static TestEntity30 CreateTestEntity30(string id)
        {
            return new TestEntity30
            {
                Id = id,
                Attr1 = RandomUtils.GenerateInt(),
                Attr2 = RandomUtils.GenerateString(),
                Attr3 = RandomUtils.GenerateInt(),
                Attr4 = RandomUtils.GenerateString(),
                Attr5 = RandomUtils.GenerateInt(),
                Attr6 = RandomUtils.GenerateString(),
                Attr7 = RandomUtils.GenerateInt(),
                Attr8 = RandomUtils.GenerateString(),
                Attr9 = RandomUtils.GenerateInt(),
                Attr10 = RandomUtils.GenerateString(),
                Attr11 = RandomUtils.GenerateInt(),
                Attr12 = RandomUtils.GenerateString(),
                Attr13 = RandomUtils.GenerateInt(),
                Attr14 = RandomUtils.GenerateString(),
                Attr15 = RandomUtils.GenerateInt(),
                Attr16 = RandomUtils.GenerateString(),
                Attr17 = RandomUtils.GenerateInt(),
                Attr18 = RandomUtils.GenerateString(),
                Attr19 = RandomUtils.GenerateInt(),
                Attr20 = RandomUtils.GenerateString(),
                Attr21 = RandomUtils.GenerateInt(),
                Attr22 = RandomUtils.GenerateString(),
                Attr23 = RandomUtils.GenerateInt(),
                Attr24 = RandomUtils.GenerateString(),
                Attr25 = RandomUtils.GenerateInt(),
                Attr26 = RandomUtils.GenerateString(),
                Attr27 = RandomUtils.GenerateInt(),
                Attr28 = RandomUtils.GenerateString(),
                Attr29 = RandomUtils.GenerateInt()
            };
        }

        private static TestEntity15 CreateTestEntity15(string id)
        {
            return new TestEntity15
            {
                Id = id,
                Attr1 = RandomUtils.GenerateInt(),
                Attr2 = RandomUtils.GenerateString(),
                Attr3 = RandomUtils.GenerateInt(),
                Attr4 = RandomUtils.GenerateString(),
                Attr5 = RandomUtils.GenerateInt(),
                Attr6 = RandomUtils.GenerateString(),
                Attr7 = RandomUtils.GenerateInt(),
                Attr8 = RandomUtils.GenerateString(),
                Attr9 = RandomUtils.GenerateInt(),
                Attr10 = RandomUtils.GenerateString(),
                Attr11 = RandomUtils.GenerateInt(),
                Attr12 = RandomUtils.GenerateString(),
                Attr13 = RandomUtils.GenerateInt(),
                Attr14 = RandomUtils.GenerateString()
            };
        }

        private static TestEntity5 CreateTestEntity5(string id)
        {
            return new TestEntity5
            {
                Id = id,
                Attr1 = RandomUtils.GenerateInt(),
                Attr2 = RandomUtils.GenerateString(),
                Attr3 = RandomUtils.GenerateInt(),
                Attr4 = RandomUtils.GenerateString()
            };
        }
    }
}
