﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    enum OperationType
    {
        Create = 'c',
        Read = 'r',
        Update = 'u',
        Delete = 'd'
    }
}
