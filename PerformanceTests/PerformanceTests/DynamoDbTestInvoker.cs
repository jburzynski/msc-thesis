﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using PerformanceTests.Entities;
using Subdivision.Core;
using Subdivision.DynamoDb;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime.Internal.Util;

namespace PerformanceTests
{
    class DynamoDbTestInvoker : ITestInvoker
    {
        private Stopwatch stopwatch;
        private TestSubdivisionContext subdivisionContext;
        private DynamoDBContext dynamoDbContext;
        private AmazonDynamoDBClient dynamoDbClient;

        private Table tableDocs;
        private Table table5;
        private Table table15;
        private Table table30;

        private const string TableNameDocs = "Docs";
        private const string TableName5 = "Test5";
        private const string TableName15 = "Test15";
        private const string TableName30 = "Test30";

        //private const string Port = "80";
        //private const string Host = "dynamodb.eu-central-1.amazonaws.com";
        //private const string HostWithPort = Host + ":" + Port;
        //private const string HostUri = "http://" + Host;
        //private const string AccessKey = "AKIAISG7QMSC4W7CR5PQ";
        //private const string SecretKey = "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";

        private const string Port = "13579";
        private const string Host = "loopback.local";
        private const string HostWithPort = Host + ":" + Port;
        private const string HostUri = "http://" + Host;
        private const string AccessKey = "dummy";
        private const string SecretKey = "dummy";

        private const string ApiVersion = "DynamoDB_20120810";

        private const string BatchWriteOpCode = "BatchWriteItem";
        private const string BatchGetOpCode = "BatchGetItem";
        private const string UpdateOpCode = "UpdateItem";

        public DynamoDbTestInvoker(Stopwatch stopwatch)
        {
            this.stopwatch = stopwatch;
            var dynamoConnectionInfo = new DynamoDbConnectionInfo
            {
                Host = Host,
                Protocol = ConnectionProtocol.Http,
                AccessKey = AccessKey,
                SecretKey = SecretKey,
                Port = int.Parse(Port)
            };
            dynamoDbClient = CreateDynamoDbClient(dynamoConnectionInfo);
            tableDocs = GetTable(TableNameDocs);
            table5 = GetTable(TableName5);
            table15 = GetTable(TableName15);
            table30 = GetTable(TableName30);

            dynamoDbContext = new DynamoDBContext(dynamoDbClient);

            subdivisionContext = new TestSubdivisionContext(dynamoConnectionInfo);
        }

        private AmazonDynamoDBClient CreateDynamoDbClient(DynamoDbConnectionInfo connectionInfo)
        {
            var config = new AmazonDynamoDBConfig { ServiceURL = connectionInfo.Url };
            var credentials = new BasicAWSCredentials(connectionInfo.AccessKey, connectionInfo.SecretKey);
            return new AmazonDynamoDBClient(credentials, config);
        }

        private Table GetTable(string tableName)
        {
            try
            {
                return Table.LoadTable(dynamoDbClient, tableName);
            }
            catch (Amazon.DynamoDBv2.Model.ResourceNotFoundException)
            {
                CreateTable(tableName);
                return Table.LoadTable(dynamoDbClient, tableName);
            }
        }

        private void CreateTable(string tableName)
        {
            var keyDefinition = new List<KeySchemaElement>
            {
                new KeySchemaElement("id", KeyType.HASH)
            };

            var attributeDefinition = new List<AttributeDefinition>
            {
                new AttributeDefinition("id", ScalarAttributeType.S)
            };

            var request = new CreateTableRequest(tableName, keyDefinition, attributeDefinition, new ProvisionedThroughput(1, 1));
            dynamoDbClient.CreateTable(request);
        }

        public void Invoke(int entitiesCount, int attributesCount, OperationType operationType, AccessMethod accessMethod)
        {
            switch (operationType)
            {
                case OperationType.Create:
                    InvokeCreate(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Read:
                    InvokeRead(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Update:
                    InvokeUpdate(entitiesCount, attributesCount, accessMethod);
                    break;
                case OperationType.Delete:
                    InvokeDelete(entitiesCount, attributesCount, accessMethod);
                    break;
            }
        }

        private void InvokeCreate(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionCreate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiCreate(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeVendorDocumentCreate(int entitiesCount, int attributesCount)
        {
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeVendorObjectCreate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectCreate5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectCreate15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectCreate30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectCreate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchPutObjects(entities);
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeVendorObjectCreate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchPutObjects(entities);
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeVendorObjectCreate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchPutObjects(entities);
            stopwatch.Stop();

            ClearTable30();
        }

        private void InvokeSubdivisionCreate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionCreate5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionCreate15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionCreate30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionCreate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeSubdivisionCreate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeSubdivisionCreate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable30();
        }

        private void InvokeWebApiCreate(int entitiesCount, int attributesCount)
        {
            string tableName = GetTableNameForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            WebApiBatchPut(documents, tableName);
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeRead(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionRead(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiRead(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeVendorDocumentRead(int entitiesCount, int attributesCount)
        {
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();
            IEnumerable<Primitive> keys = documents.Select(d => new Primitive(d["id"])).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchGetDocuments(keys, table).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeVendorObjectRead(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectRead5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectRead15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectRead30(entitiesCount);
                    break;
            }
        }
        private void InvokeVendorObjectRead5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            BatchPutObjects(entities);
            IList<string> keys = entities.Select(e => e.Id).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchGetObjects<TestEntity5>(keys);
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeVendorObjectRead15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            BatchPutObjects(entities);
            IList<string> keys = entities.Select(e => e.Id).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchGetObjects<TestEntity15>(keys);
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeVendorObjectRead30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            BatchPutObjects(entities);
            IList<string> keys = entities.Select(e => e.Id).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchGetObjects<TestEntity30>(keys);
            stopwatch.Stop();

            ClearTable30();
        }


        private void InvokeSubdivisionRead(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionRead5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionRead15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionRead30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionRead5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities5.GetAll())
            {
                // iteration
            }
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeSubdivisionRead15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities15.GetAll())
            {
                // iteration
            }
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeSubdivisionRead30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in subdivisionContext.TestEntities30.GetAll())
            {
                // iteration
            }
            stopwatch.Stop();

            ClearTable30();
        }

        private void InvokeWebApiRead(int entitiesCount, int attributesCount)
        {
            string tableName = GetTableNameForAttributesCount(attributesCount);
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();
            List<string> keys = documents.Select(d => (string)(new Primitive(d["id"]))).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            WebApiBatchGet(keys, tableName);
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeUpdate(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            InvokeCreate(entitiesCount, attributesCount, accessMethod);

            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionUpdate(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiUpdate(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeVendorDocumentUpdate(int entitiesCount, int attributesCount)
        {
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var document in documents)
            {
                table.UpdateItem(document);
            }
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeVendorObjectUpdate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectUpdate5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectUpdate15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectUpdate30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectUpdate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            BatchPutObjects(entities);
            foreach (var entity in entities)
            {
                entity.Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entities)
            {
                dynamoDbContext.Save(entity);
            }
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeVendorObjectUpdate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            BatchPutObjects(entities);
            foreach (var entity in entities)
            {
                entity.Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entities)
            {
                dynamoDbContext.Save(entity);
            }
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeVendorObjectUpdate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            BatchPutObjects(entities);
            foreach (var entity in entities)
            {
                entity.Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var entity in entities)
            {
                dynamoDbContext.Save(entity);
            }
            stopwatch.Stop();

            ClearTable30();
        }

        private void InvokeSubdivisionUpdate(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionUpdate5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionUpdate15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionUpdate30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionUpdate5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable5();
        }

        private void InvokeSubdivisionUpdate15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable15();
        }

        private void InvokeSubdivisionUpdate30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();
            for (int i = 0; i < entitiesCount; i++)
            {
                entities[i].Attr2 = "asdf";
            }

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.SaveChanges();
            stopwatch.Stop();

            ClearTable30();
        }

        private void InvokeWebApiUpdate(int entitiesCount, int attributesCount)
        {
            Table table = GetTableForAttributesCount(attributesCount);
            string tableName = GetTableNameForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();

            stopwatch.Reset();
            stopwatch.Start();
            foreach (var document in documents)
            {
                WebApiUpdateItem(document, tableName);
            }
            stopwatch.Stop();

            ClearAllTables();
        }

        private void InvokeDelete(int entitiesCount, int attributesCount, AccessMethod accessMethod)
        {
            switch (accessMethod)
            {
                case AccessMethod.VendorDocument:
                    InvokeVendorDocumentDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.VendorObject:
                    InvokeVendorObjectDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.Subdivision:
                    InvokeSubdivisionDelete(entitiesCount, attributesCount);
                    break;
                case AccessMethod.WebApi:
                    InvokeWebApiDelete(entitiesCount, attributesCount);
                    break;
            }
        }

        private void InvokeVendorDocumentDelete(int entitiesCount, int attributesCount)
        {
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();
            IEnumerable<Primitive> keys = documents.Select(d => new Primitive(d["id"])).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            BatchDeleteDocuments(keys, table).ConfigureAwait(false).GetAwaiter().GetResult();
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeVendorObjectDelete5(entitiesCount);
                    break;
                case 15:
                    InvokeVendorObjectDelete15(entitiesCount);
                    break;
                case 30:
                    InvokeVendorObjectDelete30(entitiesCount);
                    break;
            }
        }

        private void InvokeVendorObjectDelete5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            BatchPutObjects(entities);

            stopwatch.Reset();
            stopwatch.Start();
            BatchDeleteObjects(entities);
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            BatchPutObjects(entities);

            stopwatch.Reset();
            stopwatch.Start();
            BatchDeleteObjects(entities);
            stopwatch.Stop();
        }

        private void InvokeVendorObjectDelete30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            BatchPutObjects(entities);

            stopwatch.Reset();
            stopwatch.Start();
            BatchDeleteObjects(entities);
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete(int entitiesCount, int attributesCount)
        {
            switch (attributesCount)
            {
                case 5:
                    InvokeSubdivisionDelete5(entitiesCount);
                    break;
                case 15:
                    InvokeSubdivisionDelete15(entitiesCount);
                    break;
                case 30:
                    InvokeSubdivisionDelete30(entitiesCount);
                    break;
            }
        }

        private void InvokeSubdivisionDelete5(int entitiesCount)
        {
            IList<TestEntity5> entities = FixturesGenerator.CreateTestEntities5(entitiesCount).ToList();
            subdivisionContext.TestEntities5.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities5.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete15(int entitiesCount)
        {
            IList<TestEntity15> entities = FixturesGenerator.CreateTestEntities15(entitiesCount).ToList();
            subdivisionContext.TestEntities15.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities15.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeSubdivisionDelete30(int entitiesCount)
        {
            IList<TestEntity30> entities = FixturesGenerator.CreateTestEntities30(entitiesCount).ToList();
            subdivisionContext.TestEntities30.AddRange(entities);
            subdivisionContext.SaveChanges();

            stopwatch.Reset();
            stopwatch.Start();
            subdivisionContext.TestEntities30.DeleteRange(entities);
            subdivisionContext.SaveChanges();
            stopwatch.Stop();
        }

        private void InvokeWebApiDelete(int entitiesCount, int attributesCount)
        {
            string tableName = GetTableNameForAttributesCount(attributesCount);
            Table table = GetTableForAttributesCount(attributesCount);
            List<Document> documents = FixturesGenerator.CreateDynamoDocuments(entitiesCount, attributesCount).ToList();
            BatchPutDocuments(documents, table).ConfigureAwait(false).GetAwaiter().GetResult();
            List<string> keys = documents.Select(d => (string)(new Primitive(d["id"]))).ToList();

            stopwatch.Reset();
            stopwatch.Start();
            WebApiBatchDelete(keys, tableName);
            stopwatch.Stop();
        }

        public void ClearAllTables()
        {
            ClearTable5();
            ClearTable15();
            ClearTable30();
            ClearTableDocs();
        }

        private void ClearTableDocs()
        {
            ClearTable(tableDocs);
        }

        private void ClearTable5()
        {
            ClearTable(table5);
        }

        private void ClearTable15()
        {
            ClearTable(table15);
        }

        private void ClearTable30()
        {
            ClearTable(table30);
        }

        private void ClearTable(Table table)
        {
            List<Document> results;
            Search search = table.Scan(new ScanFilter());

            do
            {
                results = search.GetNextSet();
                search.Matches.Clear();
                foreach (Document document in results)
                {
                    table.DeleteItem(document);
                }
            } while (results.Any());
        }

        private string GetTableNameForAttributesCount(int attributesCount)
        {
            switch (attributesCount)
            {
                default:
                case 5:
                    return TableName5;
                case 15:
                    return TableName15;
                case 30:
                    return TableName30;
            }
        }

        private Table GetTableForAttributesCount(int attributesCount)
        {
            switch (attributesCount)
            {
                default:
                case 5:
                    return table5;
                case 15:
                    return table15;
                case 30:
                    return table30;
            }
        }

        private static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        private static string ComputeSha256(string input)
        {
            SHA256 crypt = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] crypto = crypt.ComputeHash(inputBytes, 0, Encoding.ASCII.GetByteCount(input));
            foreach (byte b in crypto)
            {
                hash.Append(b.ToString("x2"));
            }
            return hash.ToString();
        }

        private string GetDateTimeString()
        {
            return DateTime.UtcNow.ToString("yyyyMMddTHHmmssZ");
        }

        private string GetDateString(string dateTimeString)
        {
            return dateTimeString.Substring(0, 8);
        }

        private string GetCanonicalRequest(string dateTimeString, string amzTarget, string payload)
        {
            string canonicalHeaders = "host:" + HostWithPort + "\n";
            canonicalHeaders += "x-amz-date:" + dateTimeString + "\n";
            canonicalHeaders += "x-amz-target:" + amzTarget + "\n";

            string signedHeaders = "host;x-amz-date;x-amz-target";

            string sha = ComputeSha256(payload).ToLowerInvariant();

            return "POST\n"
                   + "/\n"
                   + "\n"
                   + canonicalHeaders + "\n"
                   + signedHeaders + "\n"
                   + sha;
        }

        private string GetStringToSign(string dateString, string dateTimeString, string canonicalRequestHash)
        {
            return "AWS4-HMAC-SHA256\n"
                   + dateTimeString + "\n"
                   + dateString + "/eu-central-1/dynamodb/aws4_request\n"
                   + canonicalRequestHash;
        }

        private byte[] GetHmacSigningKey(string dateString)
        {
            var dateHmac = new HMACSHA256(Encoding.ASCII.GetBytes("AWS4" + SecretKey));
            byte[] kDate = dateHmac.ComputeHash(Encoding.ASCII.GetBytes(dateString));

            var regionHmac = new HMACSHA256(kDate);
            byte[] kRegion = regionHmac.ComputeHash(Encoding.ASCII.GetBytes("eu-central-1"));

            var serviceHmac = new HMACSHA256(kRegion);
            byte[] kService = serviceHmac.ComputeHash(Encoding.ASCII.GetBytes("dynamodb"));

            var signingHmac = new HMACSHA256(kService);

            return signingHmac.ComputeHash(Encoding.ASCII.GetBytes("aws4_request"));
        }

        private string GetSignatureV4(string dateTimeString, string amzTarget, string payload)
        {
            string dateString = GetDateString(dateTimeString);

            string canonicalRequest = GetCanonicalRequest(dateTimeString, amzTarget, payload);
            string canonicalRequestHash = ComputeSha256(canonicalRequest).ToLowerInvariant();

            string stringToSign = GetStringToSign(dateString, dateTimeString, canonicalRequestHash);

            byte[] hmacSigningKey = GetHmacSigningKey(dateString);
            var signatureHmac = new HMACSHA256(hmacSigningKey);

            byte[] signatureBytes = signatureHmac.ComputeHash(Encoding.ASCII.GetBytes(stringToSign));

            return ByteArrayToString(signatureBytes).ToLowerInvariant();
        }

        private string GetAuthorizationHeader(string dateTimeString, string amzTarget, string payload)
        {
            string dateString = GetDateString(dateTimeString);
            string signature = GetSignatureV4(dateTimeString, amzTarget, payload);
            return string.Format("AWS4-HMAC-SHA256 Credential={0}/{1}/eu-central-1/dynamodb/aws4_request,SignedHeaders=host;x-amz-date;x-amz-target,Signature={2}", AccessKey, dateString, signature);
        }

        private string GetAmzTarget(string operationType)
        {
            return ApiVersion + "." + operationType;
        }

        private HttpWebRequest CreateHttpWebRequest(string amzTarget, string payload)
        {
            string dateTimeString = GetDateTimeString();

            var request = (HttpWebRequest)WebRequest.Create(HostUri + ":" + Port);

            request.Method = "POST";
            request.Host = HostWithPort;
            request.ContentType = "application/x-amz-json-1.0";
            request.Accept = "application/json";
            request.Headers.Add("x-amz-date", dateTimeString);
            request.Headers.Add("x-amz-target", amzTarget);
            request.Headers.Add("Authorization", GetAuthorizationHeader(dateTimeString, amzTarget, payload));

            request.GetRequestStream().Write(Encoding.ASCII.GetBytes(payload), 0, payload.Length);

            return request;
        }

        private string GetTypeString(DynamoDBEntryType type)
        {
            switch (type)
            {
                default:
                case DynamoDBEntryType.String:
                    return "S";
                case DynamoDBEntryType.Binary:
                    return "B";
                case DynamoDBEntryType.Numeric:
                    return "N";
            }
        }

        private void WebApiUpdateItem(Document document, string tableName)
        {
            string updateExpression = "set ";
            string expressionAttributeValues = "{";

            List<string> attributeNames = document.GetAttributeNames();
            for (int i = 1; i < attributeNames.Count; i++)
            {
                string type = i % 2 == 0 ? "S" : "N";
                object value;
                if (i % 2 == 0)
                {
                    value = "asdf";
                }
                else
                {
                    value = 123;
                }
                updateExpression += attributeNames[i] + "=:value" + i + ", ";
                expressionAttributeValues += "\":value" + i + "\": { \"" + type + "\": \"" + value + "\" }, ";
            }
            updateExpression = updateExpression.Remove(updateExpression.Length - 2);
            expressionAttributeValues = expressionAttributeValues.Remove(expressionAttributeValues.Length - 2);
            expressionAttributeValues += "}";

            string requestJson = "{\"TableName\":\"" + tableName + "\",\"Key\":{\"id\":{\"S\":\"" + document["id"] + "\"}},"
                                 + "\"UpdateExpression\":\"" + updateExpression + "\","
                                 + "\"ExpressionAttributeValues\": " + expressionAttributeValues
                                 + "}";

            HttpWebRequest request = CreateHttpWebRequest(GetAmzTarget(UpdateOpCode), requestJson);
            WebResponse response = request.GetResponse();
            request.GetRequestStream().Close();
            response.Close();
        }

        private void WebApiBatchGet(List<string> entityKeys, string tableName)
        {
            List<List<string>> chunkedKeys = entityKeys.SplitToChunks(25);
            foreach (List<string> keys in chunkedKeys)
            {
                string requestJson = "{\"RequestItems\":{\"" + tableName + "\":{\"Keys\":[";
                foreach (string key in keys)
                {
                    requestJson += "{\"id\":{\"S\":\"" + key + "\"}},";
                }
                requestJson = requestJson.Remove(requestJson.Length - 1);
                requestJson += "]}}}";

                HttpWebRequest request = CreateHttpWebRequest(GetAmzTarget(BatchGetOpCode), requestJson);
                WebResponse response = request.GetResponse();
                request.GetRequestStream().Close();
                response.Close();
            }
        }

        private void WebApiBatchPut(List<Document> documents, string tableName)
        {
            List<List<Document>> chunkedDocuments = documents.SplitToChunks(25);
            foreach (List<Document> documentsList in chunkedDocuments)
            {
                string requestJson = "{\"RequestItems\":{\"" + tableName + "\":[";
                foreach (Document document in documentsList)
                {
                    requestJson += "{\"PutRequest\":{\"Item\":{";
                    foreach (string attributeName in document.GetAttributeNames())
                    {
                        string type = GetTypeString(new Primitive(document[attributeName]).Type);
                        requestJson += "\"" + attributeName + "\":{\"" + type + "\":\"" + document[attributeName] + "\"},";
                    }
                    requestJson = requestJson.Remove(requestJson.Length - 1);
                    requestJson += "}}},";
                }
                requestJson = requestJson.Remove(requestJson.Length - 1);
                requestJson += "]}}";

                HttpWebRequest request = CreateHttpWebRequest(GetAmzTarget(BatchWriteOpCode), requestJson);
                WebResponse response = request.GetResponse();
                request.GetRequestStream().Close();
                response.Close();
            }
        }

        private void WebApiBatchDelete(List<string> entityKeys, string tableName)
        {
            List<List<string>> chunkedKeys = entityKeys.SplitToChunks(25);
            foreach (List<string> keys in chunkedKeys)
            {
                string requestJson = "{\"RequestItems\":{\"" + tableName + "\":[";
                foreach (string key in keys)
                {
                    requestJson += "{\"DeleteRequest\":{\"Key\":{\"id\":{\"S\":\"" + key + "\"}}}},";
                }
                requestJson = requestJson.Remove(requestJson.Length - 1);
                requestJson += "]}}";

                HttpWebRequest request = CreateHttpWebRequest(GetAmzTarget(BatchWriteOpCode), requestJson);
                WebResponse response = request.GetResponse();
                request.GetRequestStream().Close();
                response.Close();
            }
        }

        private void BatchGetObjects<TEntity>(IEnumerable<string> entityKeys)
        {
            BatchGet<TEntity> batchGet = dynamoDbContext.CreateBatchGet<TEntity>();
            var fetchedEntities = new List<TEntity>();
            int i = 0;
            foreach (string key in entityKeys)
            {
                batchGet.AddKey(key);

                i++;
                if (i == 100)
                {
                    i = 0;
                    batchGet.Execute();
                    fetchedEntities.AddRange(batchGet.Results);
                    batchGet = dynamoDbContext.CreateBatchGet<TEntity>();
                }
            }

            if (i > 0)
            {
                batchGet.Execute();
                fetchedEntities.AddRange(batchGet.Results);
            }
        }

        private void BatchPutObjects<TEntity>(IEnumerable<TEntity> entities)
        {
            BatchWrite<TEntity> batchWrite = dynamoDbContext.CreateBatchWrite<TEntity>();
            int i = 0;
            foreach (TEntity entity in entities)
            {
                batchWrite.AddPutItem(entity);

                i++;
                if (i == 25)
                {
                    i = 0;
                    batchWrite.Execute();
                    batchWrite = dynamoDbContext.CreateBatchWrite<TEntity>();
                }
            }

            if (i > 0)
            {
                batchWrite.Execute();
            }
        }

        private void BatchDeleteObjects<TEntity>(IEnumerable<TEntity> entities)
        {
            BatchWrite<TEntity> batchWrite = dynamoDbContext.CreateBatchWrite<TEntity>();
            int i = 0;
            foreach (TEntity entity in entities)
            {
                batchWrite.AddDeleteItem(entity);

                i++;
                if (i == 25)
                {
                    i = 0;
                    batchWrite.Execute();
                    batchWrite = dynamoDbContext.CreateBatchWrite<TEntity>();
                }
            }

            if (i > 0)
            {
                batchWrite.Execute();
            }
        }

        private async Task<IList<Document>> BatchGetDocuments(IEnumerable<Primitive> entityKeys, Table table)
        {
            DocumentBatchGet batchGet = table.CreateBatchGet();
            var fetchedDocuments = new List<Document>();
            int i = 0;
            foreach (Primitive entityKey in entityKeys)
            {
                batchGet.AddKey(entityKey);

                i++;
                if (i == 100)
                {
                    i = 0;
                    await batchGet.ExecuteAsync();
                    fetchedDocuments.AddRange(batchGet.Results);
                    batchGet = table.CreateBatchGet();
                }
            }

            if (i > 0)
            {
                await batchGet.ExecuteAsync();
                fetchedDocuments.AddRange(batchGet.Results);
            }

            return fetchedDocuments;
        }

        private async Task BatchPutDocuments(IEnumerable<Document> documents, Table table)
        {
            DocumentBatchWrite batchWrite = table.CreateBatchWrite();
            int i = 0;
            foreach (Document document in documents)
            {
                batchWrite.AddDocumentToPut(document);

                i++;
                if (i == 25)
                {
                    i = 0;
                    await batchWrite.ExecuteAsync();
                    batchWrite = table.CreateBatchWrite();
                }
            }

            if (i > 0)
            {
                await batchWrite.ExecuteAsync();
            }
        }

        private async Task BatchDeleteDocuments(IEnumerable<Primitive> entityKeys, Table table)
        {
            DocumentBatchWrite batchWrite = table.CreateBatchWrite();
            int i = 0;
            foreach (Primitive keyPrimitive in entityKeys)
            {
                batchWrite.AddKeyToDelete(keyPrimitive);

                i++;
                if (i == 25)
                {
                    i = 0;
                    await batchWrite.ExecuteAsync();
                    batchWrite = table.CreateBatchWrite();
                }
            }

            if (i > 0)
            {
                await batchWrite.ExecuteAsync();
            }
        }

        private class TestSubdivisionContext : DynamoDbContext
        {
            public TestSubdivisionContext(DynamoDbConnectionInfo connectionInfo)
                : base(connectionInfo)
            {
            }

            public IEntitySet<TestEntity5> TestEntities5 { get; set; }
            public IEntitySet<TestEntity15> TestEntities15 { get; set; }
            public IEntitySet<TestEntity30> TestEntities30 { get; set; }
        }
    }
}
