﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    class Program
    {
        private static readonly Stopwatch stopwatch;
        private static readonly MongoDbTestInvoker mongoDbTestInvoker;
        private static readonly DynamoDbTestInvoker dynamoDbTestInvoker;
        private static string resultsFileName;

        static Program()
        {
            stopwatch = new Stopwatch();
            mongoDbTestInvoker = new MongoDbTestInvoker(stopwatch);
            dynamoDbTestInvoker = new DynamoDbTestInvoker(stopwatch);
        }

        private const string ExportAllModeSwitch = "/ea";
        private const string ExportSingleModeSwitch = "/es";

        private static void RunExportSingle(string fileName, string entitiesCountSymbol = "a", string attributesCountSymbol = "a", string operationSymbol = "c", string engineSymbol = "a", string accessMethodSymbol = "a")
        {
            resultsFileName = fileName;

            int entitiesCount = int.Parse(EntitiesSymbolToReadableName(entitiesCountSymbol).Replace(" ", string.Empty));
            int attributesCount = int.Parse(AttributesSymbolToReadableName(attributesCountSymbol));
            OperationType operationType = (OperationType)operationSymbol.First();
            AccessMethod accessMethod = (AccessMethod)accessMethodSymbol.First();

            Console.WriteLine("Execution plan:");
            Console.WriteLine("# entities: {0}", entitiesCount);
            Console.WriteLine("# attributes: {0}", attributesCount);
            Console.WriteLine("Operation type: {0}", OperationSymbolToReadableName(operationSymbol));
            Console.WriteLine("Engine: {0}", EngineSymbolToReadableName(engineSymbol));
            Console.WriteLine("Access method: {0}", AccessMethodSymbolToReadableName(accessMethodSymbol));
            Console.WriteLine();

            ITestInvoker testInvoker;
            if (engineSymbol == "a")
            {
                testInvoker = mongoDbTestInvoker;
            }
            else
            {
                testInvoker = dynamoDbTestInvoker;
            }

            long preMaxMemoryUsage = Process.GetCurrentProcess().PeakWorkingSet64;

            Console.WriteLine("Starting measurement (10 iterations)...");

            const int trysCount = 10;
            long timeSum = 0;
            for (int i = 0; i < trysCount; i++)
            {
                testInvoker.Invoke(entitiesCount, attributesCount, operationType, accessMethod);
                timeSum += stopwatch.ElapsedMilliseconds;
            }
            long timeMean = timeSum / trysCount;

            long maxMemoryUsage = Process.GetCurrentProcess().PeakWorkingSet64 - preMaxMemoryUsage;

            Console.WriteLine("Measurement finished. Exporting to CSV...");

            var csvFile = new CsvFile(fileName);
            csvFile.AddResult(
                EngineSymbolToReadableName(engineSymbol),
                AccessMethodSymbolToReadableName(accessMethodSymbol),
                OperationSymbolToReadableName(operationSymbol),
                entitiesCount,
                attributesCount,
                timeMean,
                maxMemoryUsage
            );
            csvFile.Save();

            Console.WriteLine("Exported. Exiting...");
        }

        private static void RunExportAll(string entitiesCountSymbol = "a", string attributesCountSymbol = "a", string operationSymbol = "c", string engineSymbol = "a", string accessMethodSymbol = "a")
        {
            resultsFileName = "results_" + DateTime.Now.ToString("yyyy_MM_dd_h_mm_ss") + ".csv";

            string[][] plan = {
                                  new[] {"a", "v", "x", "b", "c", "d"/*, "e"*/}, // NOTE: 1 000 000 entities --> out of memory exception
                                  new[] {"a", "b", "c"},
                                  new[] {"c", "r", "u", "d"},
                                  new[] {"a", "b"},
                                  new[] {"a", "b", "c", "d", "e"}
                             };

            int entitiesRow = 0;
            int attributesRow = 1;
            int operationRow = 2;
            int engineRow = 3;
            int accessMethodRow = 4;

            int entitiesIndexStart = plan[0].ToList().IndexOf(entitiesCountSymbol);
            int attributesIndexStart = plan[1].ToList().IndexOf(attributesCountSymbol);
            int operationIndexStart = plan[2].ToList().IndexOf(operationSymbol);
            int engineIndexStart = plan[3].ToList().IndexOf(engineSymbol);
            int accessMethodIndexStart = plan[4].ToList().IndexOf(accessMethodSymbol);

            for (int engineIndex = engineIndexStart; engineIndex < plan[engineRow].Length; engineIndex++)
            {
                for (int accessMethodIndex = accessMethodIndexStart; accessMethodIndex < plan[accessMethodRow].Length; accessMethodIndex++)
                {
                    // don't execute: PubComp for DynamoDB
                    if (plan[engineRow][engineIndex] == "b" && plan[accessMethodRow][accessMethodIndex] == "d")
                    {
                        continue;
                    }

                    for (int operationIndex = operationIndexStart; operationIndex < plan[operationRow].Length; operationIndex++)
                    {
                        for (int entitiesIndex = entitiesIndexStart; entitiesIndex < plan[entitiesRow].Length; entitiesIndex++)
                        {
                            // don't execute: 100k records for Subdivision Update
                            //if (plan[entitiesRow][entitiesIndex] == "d" && plan[accessMethodRow][accessMethodIndex] == "c" && plan[operationRow][operationIndex] == "u")
                            //{
                            //    continue;
                            //}

                            // don't execute: 10k and 100k records for DynamoDB
                            if ((plan[entitiesRow][entitiesIndex] == "d" || plan[entitiesRow][entitiesIndex] == "c") && plan[engineRow][engineIndex] == "b")
                            {
                                continue;
                            }

                            for (int attributesIndex = attributesIndexStart; attributesIndex < plan[attributesRow].Length; attributesIndex++)
                            {
                                Process process = Process.Start("PerformanceTests.exe", 
                                    string.Format("{0} {1} {2} {3} {4} {5} {6}",
                                        resultsFileName,
                                        plan[entitiesRow][entitiesIndex],
                                        plan[attributesRow][attributesIndex],
                                        plan[operationRow][operationIndex],
                                        plan[engineRow][engineIndex],
                                        plan[accessMethodRow][accessMethodIndex],
                                        ExportSingleModeSwitch
                                    )
                                );
                                process.WaitForExit();
                            }
                        }
                    }
                }
            }
        }

        private static void RunDebug()
        {
            while (true)
            {
                Console.WriteLine("Choose # entities: [a]");
                Console.WriteLine("a - {0}", EntitiesSymbolToReadableName("a"));
                Console.WriteLine("v - {0}", EntitiesSymbolToReadableName("v"));
                Console.WriteLine("x - {0}", EntitiesSymbolToReadableName("x"));
                Console.WriteLine("b - {0}", EntitiesSymbolToReadableName("b"));
                Console.WriteLine("c - {0}", EntitiesSymbolToReadableName("c"));
                Console.WriteLine("d - {0}", EntitiesSymbolToReadableName("d"));
                Console.WriteLine("e - {0}", EntitiesSymbolToReadableName("e"));
                Console.WriteLine();
                Console.WriteLine("or type \"z\" to clear DynamoDB tables.");
                string entitiesCountString = Console.ReadLine();

                if (entitiesCountString == "z")
                {
                    dynamoDbTestInvoker.ClearAllTables();
                    continue;
                }

                if (entitiesCountString == string.Empty)
                {
                    entitiesCountString = "a";
                }

                Console.WriteLine("Choose # attributes: [a]");
                Console.WriteLine("a - {0}", AttributesSymbolToReadableName("a"));
                Console.WriteLine("b - {0}", AttributesSymbolToReadableName("b"));
                Console.WriteLine("c - {0}", AttributesSymbolToReadableName("c"));
                string attributesCountString = Console.ReadLine();
                if (attributesCountString == string.Empty)
                {
                    attributesCountString = "a";
                }

                Console.WriteLine("Choose operation type (c, r, u, d): [c]");
                string operationTypeString = Console.ReadLine();
                if (operationTypeString == string.Empty)
                {
                    operationTypeString = "c";
                }

                Console.WriteLine("Choose engine: [a]");
                Console.WriteLine("a - {0}", EngineSymbolToReadableName("a"));
                Console.WriteLine("b - {0}", EngineSymbolToReadableName("b"));
                string engineTypeString = Console.ReadLine();
                if (engineTypeString == string.Empty)
                {
                    engineTypeString = "a";
                }

                Console.WriteLine("Choose access method: [a]");
                Console.WriteLine("a - {0}", AccessMethodSymbolToReadableName("a"));
                Console.WriteLine("b - {0}", AccessMethodSymbolToReadableName("b"));
                Console.WriteLine("c - {0}", AccessMethodSymbolToReadableName("c"));
                Console.WriteLine("d - {0}", AccessMethodSymbolToReadableName("d"));
                Console.WriteLine("e - {0}", AccessMethodSymbolToReadableName("e"));
                string accessMethodString = Console.ReadLine();
                if (accessMethodString == string.Empty)
                {
                    accessMethodString = "a";
                }

                Console.WriteLine("Summary:");
                Console.WriteLine("# entities: {0}", EntitiesSymbolToReadableName(entitiesCountString));
                Console.WriteLine("# attributes: {0}", AttributesSymbolToReadableName(attributesCountString));
                Console.WriteLine("Operation type: {0}", OperationSymbolToReadableName(operationTypeString));
                Console.WriteLine("Engine: {0}", EngineSymbolToReadableName(engineTypeString));
                Console.WriteLine("Access method: {0}", AccessMethodSymbolToReadableName(accessMethodString));
                Console.WriteLine("Proceed with execution (y/n)? [y]");

                if (Console.ReadLine().ToUpperInvariant() == "N")
                {
                    continue;
                }

                int entitiesCount = int.Parse(EntitiesSymbolToReadableName(entitiesCountString).Replace(" ", string.Empty));
                int attributesCount = int.Parse(AttributesSymbolToReadableName(attributesCountString));
                OperationType operationType = (OperationType)operationTypeString.First();
                AccessMethod accessMethod = (AccessMethod)accessMethodString.First();

                ITestInvoker testInvoker;
                if (engineTypeString == "a")
                {
                    testInvoker = mongoDbTestInvoker;
                }
                else
                {
                    testInvoker = dynamoDbTestInvoker;
                }
                testInvoker.Invoke(entitiesCount, attributesCount, operationType, accessMethod);

                Console.WriteLine("Execution time: {0} ms", stopwatch.ElapsedMilliseconds);

                Console.WriteLine("Maximum memory usage so far: {0} B", Process.GetCurrentProcess().PeakWorkingSet64);
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            if ((args.Length == 1 || args.Length == 6) && args.Last() == ExportAllModeSwitch)
            {
                Console.WriteLine("--EXPORT MODE (ALL RESULTS)--");
                Console.WriteLine();
                if (args.Length == 6)
                {
                    RunExportAll(args[0], args[1], args[2], args[3], args[4]);
                }
                else
                {
                    RunExportAll();
                }
            }
            else if (args.Length == 7 && args.Last() == ExportSingleModeSwitch)
            {
                Console.WriteLine("--EXPORT MODE (SINGLE RESULT)--");
                Console.WriteLine();
                RunExportSingle(args[0], args[1], args[2], args[3], args[4], args[5]);
            }
            else
            {
                Console.WriteLine("--DEBUG MODE--");
                Console.WriteLine();
                RunDebug();
            }
        }

        private static string EntitiesSymbolToReadableName(string symbol)
        {
            switch(symbol)
            {
                default:
                case "a":
                    return "100";
                case "b":
                    return "1 000";
                case "c":
                    return "10 000";
                case "d":
                    return "100 000";
                case "e":
                    return "1 000 000";
                case "v":
                    return "300";
                case "x":
                    return "600";
            }
        }

        private static string AttributesSymbolToReadableName(string symbol)
        {
            switch(symbol)
            {
                default:
                case "a":
                    return "5";
                case "b":
                    return "15";
                case "c":
                    return "30";
            }
        }

        private static string OperationSymbolToReadableName(string symbol)
        {
            switch (symbol)
            {
                default:
                case "c":
                    return "Create";
                case "r":
                    return "Read";
                case "u":
                    return "Update";
                case "d":
                    return "Delete";
            }
        }

        private static string EngineSymbolToReadableName(string symbol)
        {
            switch (symbol)
            {
                default:
                case "a":
                    return "MongoDB";
                case "b":
                    return "DynamoDB";
            }
        }

        private static string AccessMethodSymbolToReadableName(string symbol)
        {
            switch (symbol)
            {
                default:
                case "a":
                    return "vendor (Document API)";
                case "b":
                    return "vendor (Object mapping)";
                case "c":
                    return "Subdivision";
                case "d":
                    return "PubComp.NoSQL";
                case "e":
                    return "web API";
            }
        }
    }
}
