﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    enum AccessMethod
    {
        VendorDocument = 'a',
        VendorObject = 'b',
        Subdivision = 'c',
        PubComp = 'd',
        WebApi = 'e'
    }
}
