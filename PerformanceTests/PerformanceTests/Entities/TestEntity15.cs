﻿using Amazon.DynamoDBv2.DataModel;
using PubComp.NoSql.Core;
using Subdivision.DynamoDb.Mapping;
using Subdivision.MongoDb.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests.Entities
{
    [Collection("test.benchmark.test15")]
    [DynamoDBTable("Test15")]
    [Table("Test15")]
    class TestEntity15 : IEntity<string>
    {
        [Id(IdType.Custom)]
        [HashKey]
        [DynamoDBHashKey("id")]
        public string Id { get; set; }
        public int Attr1 { get; set; }
        public string Attr2 { get; set; }
        public int Attr3 { get; set; }
        public string Attr4 { get; set; }
        public int Attr5 { get; set; }
        public string Attr6 { get; set; }
        public int Attr7 { get; set; }
        public string Attr8 { get; set; }
        public int Attr9 { get; set; }
        public string Attr10 { get; set; }
        public int Attr11 { get; set; }
        public string Attr12 { get; set; }
        public int Attr13 { get; set; }
        public string Attr14 { get; set; }
    }
}
