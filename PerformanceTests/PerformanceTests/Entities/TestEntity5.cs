﻿using Amazon.DynamoDBv2.DataModel;
using PubComp.NoSql.Core;
using Subdivision.DynamoDb.Mapping;
using Subdivision.MongoDb.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests.Entities
{
    [Collection("test.benchmark.test5")]
    [DynamoDBTable("Test5")]
    [Table("Test5")]
    class TestEntity5 : IEntity<string>
    {
        [Id(IdType.Custom)]
        [HashKey]
        [DynamoDBHashKey("id")]
        public string Id { get; set; }
        public int Attr1 { get; set; }
        public string Attr2 { get; set; }
        public int Attr3 { get; set; }
        public string Attr4 { get; set; }
    }
}
