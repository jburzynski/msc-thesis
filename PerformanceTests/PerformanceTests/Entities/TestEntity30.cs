﻿using Amazon.DynamoDBv2.DataModel;
using PubComp.NoSql.Core;
using Subdivision.DynamoDb.Mapping;
using Subdivision.MongoDb.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests.Entities
{
    [Collection("test.benchmark.test30")]
    [DynamoDBTable("Test30")]
    [Table("Test30")]
    class TestEntity30 : IEntity<string>
    {
        [Id(IdType.Custom)]
        [HashKey]
        [DynamoDBHashKey("id")]
        public string Id { get; set; }
        public int Attr1 { get; set; }
        public string Attr2 { get; set; }
        public int Attr3 { get; set; }
        public string Attr4 { get; set; }
        public int Attr5 { get; set; }
        public string Attr6 { get; set; }
        public int Attr7 { get; set; }
        public string Attr8 { get; set; }
        public int Attr9 { get; set; }
        public string Attr10 { get; set; }
        public int Attr11 { get; set; }
        public string Attr12 { get; set; }
        public int Attr13 { get; set; }
        public string Attr14 { get; set; }
        public int Attr15 { get; set; }
        public string Attr16 { get; set; }
        public int Attr17 { get; set; }
        public string Attr18 { get; set; }
        public int Attr19 { get; set; }
        public string Attr20 { get; set; }
        public int Attr21 { get; set; }
        public string Attr22 { get; set; }
        public int Attr23 { get; set; }
        public string Attr24 { get; set; }
        public int Attr25 { get; set; }
        public string Attr26 { get; set; }
        public int Attr27 { get; set; }
        public string Attr28 { get; set; }
        public int Attr29 { get; set; }
    }
}
