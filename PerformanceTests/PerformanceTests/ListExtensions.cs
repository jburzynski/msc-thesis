﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTests
{
    static class ListExtensions
    {
        public static List<List<T>> SplitToChunks<T>(this List<T> list, int chunkSize)
        {
            var result = new List<List<T>>();

            for (int i = 0; i < list.Count; i += chunkSize)
            {
                result.Add(list.GetRange(i, Math.Min(chunkSize, list.Count - i)));
            }

            return result;
        } 
    }
}
