https://drive.google.com/folderview?id=0B_MTcr5XTUIPfjIyWkJWUWhXX09JV2NiSDM2WnA2NFNSWjdRUDFySU8wS3pzRUQzUlh5czA&usp=sharing
http://www.weka.pwr.edu.pl/files/prv/id4/dyplomanci/INF_IIM_pyt.pdf

1.XSLT concept, area of applications. Describe language directives.
2.XML documents processing in Java: describe and compare available techniques.
3.The role of XML-based service description languages in the creation of multilayer information systems.
4.The requirements and tasks of the main design patterns of each layer of the multilayer information
systems.
5.Information systems analysis using Petri nets.
6. Model checking using temporal logic.
7.Privacy, access control and security management in relational database management systems.
8.XML extensions to relational database management systems and non-relational databases.
9.Residual arithmetic and its applications.
10.Enterprise and corporate applications - Characteristics and technical aspects.
11.Payment card transactions: types of transactions, technological solutions, security.
12.Authentication methods in computer systems
13.Security problems related to network communication.
14.Artificial neural networks: learning algorithms.
15.Genetic algorithms and expert systems - main features and applications.
16.Describe the color model "luminance-chrominance" and its application.
17.Discuss the JPEG compression algorithm.
18.Data warehouse – purpose, characteristics and architectures.
19.Purpose and short characteristics of main methods of data mining.
20.Characteristic limitations of mobile systems related to hardware, software, user interface and
networking 