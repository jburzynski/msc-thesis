﻿using MongoDB.Bson;

namespace MongoDBTest
{
    public class Movie
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
    }
}
