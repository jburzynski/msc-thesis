﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace MongoDBTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(typeof(Movie).Namespace);
            //Console.ReadLine();

            // http://docs.mongodb.org/manual/core/introduction/ "key features"

            string connectionString = "mongodb://localhost:27017";
            var client = new MongoClient(connectionString);
            var db = client.GetDatabase("test");

            Console.WriteLine("Before:");

            IMongoCollection<Movie> movies = db.GetCollection<Movie>("test.tutorial.movies");
            //foreach (Movie movie in movies.FindAll())
            //{
            //    Console.WriteLine("Name: {0}, Duration: {1}", movie.Name, movie.Duration);
            //}

            Console.WriteLine();
            Console.WriteLine("After:");

            var ml = movies.Find(new BsonDocument()).ToListAsync().Result;

            //movies.InsertOneAsync(new Movie() {Name = "asdf", Duration = 123});

            //using (var cursor = movies.Find(new BsonDocument()).ToCursorAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            //{
            //    while (cursor.MoveNextAsync().ConfigureAwait(false).GetAwaiter().GetResult())
            //    {
            //        var a = cursor.Current;
            //    }
            //}

            //movies.Save(new Movie { Name = "Aliens", Duration = 125 });
            //foreach (Movie movie in movies.FindAll())
            //{
            //    Console.WriteLine("Name: {0}, Duration: {1}", movie.Name, movie.Duration);
            //}

            DbSet<Movie> moviesSet;

            Console.ReadLine();
        }
    }
}
