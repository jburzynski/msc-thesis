﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.DynamoDBv2.DocumentModel;

namespace Subdivision.DynamoDb
{
    public static class PrimitiveExtensions
    {
        private static readonly IList<Type> stringDataTypes;
        private static readonly IList<Type> numericDataTypes;

        static PrimitiveExtensions()
        {
            stringDataTypes = new List<Type> { typeof(char), typeof(string), typeof(DateTime), typeof(Guid) };
            numericDataTypes = new List<Type> { typeof(ushort), typeof(short), typeof(uint), typeof(int), typeof(ulong), typeof(long), typeof(float), typeof(double), typeof(decimal) };
        }

        public static object ToObject(this Primitive primitive, Type type)
        {
            object result;

            if (type == typeof(ushort))
            {
                result = (ushort)primitive;
            }
            else if (type == typeof(short))
            {
                result = (short)primitive;
            }
            else if (type == typeof(uint))
            {
                result = (uint)primitive;
            }
            else if (type == typeof(int))
            {
                result = (int)primitive;
            }
            else if (type == typeof(ulong))
            {
                result = (ulong)primitive;
            }
            else if (type == typeof(long))
            {
                result = (long)primitive;
            }
            else if (type == typeof(float))
            {
                result = (float)primitive;
            }
            else if (type == typeof(double))
            {
                result = (double)primitive;
            }
            else if (type == typeof(decimal))
            {
                result = (decimal)primitive;
            }
            else if (type == typeof(char))
            {
                result = (char)primitive;
            }
            else if (type == typeof(DateTime))
            {
                result = (DateTime)primitive;
            }
            else if (type == typeof(Guid))
            {
                result = (Guid)primitive;
            }
            else if (type == typeof(bool))
            {
                result = (bool)primitive;
            }
            else if (type == typeof(byte))
            {
                result = (byte)primitive;
            }
            else if (type == typeof(sbyte))
            {
                result = (sbyte)primitive;
            }
            else if (type == typeof(byte[]))
            {
                result = (byte[])primitive;
            }
            else if (type == typeof(MemoryStream))
            {
                result = (MemoryStream)primitive;
            }
            else
            {
                result = (string)primitive;
            }

            return result;
        }

        public static Primitive ToPrimitive(this object value)
        {
            DynamoDBEntryType entryType;
            Type fieldType = value.GetType();
            if (stringDataTypes.Contains(fieldType))
            {
                entryType = DynamoDBEntryType.String;
            }
            else if (numericDataTypes.Contains(fieldType))
            {
                entryType = DynamoDBEntryType.Numeric;
                value = value.ToString();
            }
            else
            {
                entryType = DynamoDBEntryType.Binary;
            }

            return new Primitive
            {
                Value = value,
                Type = entryType
            };
        }
    }
}
