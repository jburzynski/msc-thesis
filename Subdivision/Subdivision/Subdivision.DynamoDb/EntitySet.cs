﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Subdivision.Core;
using Subdivision.DynamoDb.Mapping;

namespace Subdivision.DynamoDb
{
    internal class EntitySet<TEntity> : IDynamoDbEntitySet, IEntitySet<TEntity> where TEntity : class
    {
        private readonly Table table;

        private readonly List<TEntity> originalEntities; 
        private readonly List<TEntity> loadedEntities;
        private readonly List<TEntity> addedEntities;
        private readonly List<TEntity> deletedEntities;

        private readonly PropertyInfo hashKeyPropertyInfo;
        private readonly IEnumerable<PropertyInfo> allPropertyInfos;

        private readonly ClassMapper<TEntity> classMapper;
        private readonly MappingResolver mappingResolver;

        public EntitySet(AmazonDynamoDBClient client)
        {
            Type entityType = typeof (TEntity);
            mappingResolver = new MappingResolver();
            classMapper = new ClassMapper<TEntity>(mappingResolver);

            hashKeyPropertyInfo = mappingResolver.GetHashKeyPropertyInfo(entityType);
            allPropertyInfos = mappingResolver.GetEntityPropertyInfos(entityType);

            originalEntities = new List<TEntity>();
            loadedEntities = new List<TEntity>();
            addedEntities = new List<TEntity>();
            deletedEntities = new List<TEntity>();

            string tableName = mappingResolver.GetTableName(entityType);
            table = Table.LoadTable(client, tableName);
        }

        private bool EntityHasKey(TEntity entity, object key)
        {
            object entityId = hashKeyPropertyInfo.GetValue(entity);
            return entityId != null && entityId.Equals(key);
        }

        public void Add(TEntity entity)
        {
            if (addedEntities.Contains(entity) || loadedEntities.Contains(entity))
            {
                throw new ContextException("The entity was already added or loaded.");
            }
            addedEntities.Add(entity);
            deletedEntities.Remove(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (addedEntities.Any(entities.Contains) || loadedEntities.Any(entities.Contains))
            {
                throw new ContextException("The entity was already added or loaded.");
            }
            addedEntities.AddRange(entities);

            foreach (TEntity entity in entities)
            {
                deletedEntities.Remove(entity);
            }
        }

        public void Attach(TEntity entity)
        {
            if (addedEntities.Contains(entity) || loadedEntities.Contains(entity))
            {
                return;
            }

            if (hashKeyPropertyInfo.GetValue(entity) != null)
            {
                loadedEntities.Add(entity);
                originalEntities.Add(entity.Clone());
                return;
            }

            addedEntities.Add(entity);
        }

        public void Detach(TEntity entity)
        {
            addedEntities.Remove(entity);
            loadedEntities.Remove(entity);
            deletedEntities.Remove(entity);
        }

        public void Delete(TEntity entity)
        {
            if (deletedEntities.Contains(entity))
            {
                throw new ContextException("The entity was already deleted.");
            }
            deletedEntities.Add(entity);
            addedEntities.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (deletedEntities.Any(entities.Contains))
            {
                throw new ContextException("The entity was already deleted.");
            }
            deletedEntities.AddRange(entities);

            foreach (TEntity entity in entities)
            {
                addedEntities.Remove(entity);
            }
        }

        public TEntity Get(object entityKey)
        {
            return GetAsync(entityKey).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public IEnumerable<TEntity> Get(IEnumerable<object> entityKeys)
        {
            return GetAsync(entityKeys).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return GetAllAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public async Task<TEntity> GetAsync(object entityKey)
        {
            if (loadedEntities.Any(e => EntityHasKey(e, entityKey)))
            {
                return loadedEntities.First(e => EntityHasKey(e, entityKey));
            }

            if (addedEntities.Any(e => EntityHasKey(e, entityKey)))
            {
                return addedEntities.First(e => EntityHasKey(e, entityKey));
            }

            Primitive keyPrimitive = entityKey.ToPrimitive();
            Document document = await table.GetItemAsync(keyPrimitive);

            if (document != null)
            {
                TEntity entity = classMapper.DocumentToEntity(document);
                loadedEntities.Add(entity);
                originalEntities.Add(entity.Clone());
                return entity;
            }

            return null;
        }

        private IList<TEntity> GetMissingEntities(IEnumerable<object> entityKeys, List<object> entityKeysToFetch)
        {
            var entities = new List<TEntity>();

            foreach (object entityKey in entityKeys)
            {
                if (!entityKeysToFetch.Contains(entityKey))
                {
                    TEntity entity = loadedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));

                    if (entity == null)
                    {
                        entity = addedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));
                    }

                    if (entity != null)
                    {
                        entities.Add(entity);
                    }
                }
            }

            return entities;
        }

        private async Task<IEnumerable<Document>> BatchGet(IEnumerable<Primitive> entityKeys)
        {
            DocumentBatchGet batchGet = table.CreateBatchGet();
            var fetchedDocuments = new List<Document>();
            int i = 0;
            foreach (Primitive entityKey in entityKeys)
            {
                batchGet.AddKey(entityKey);

                i++;
                if (i == 100)
                {
                    i = 0;
                    await batchGet.ExecuteAsync();
                    fetchedDocuments.AddRange(batchGet.Results);
                    batchGet = table.CreateBatchGet();
                }
            }

            if (i > 0)
            {
                await batchGet.ExecuteAsync();
                fetchedDocuments.AddRange(batchGet.Results);
            }

            return fetchedDocuments;
        }

        private async Task BatchPut(IEnumerable<Document> documents)
        {
            DocumentBatchWrite batchWrite = table.CreateBatchWrite();
            int i = 0;
            foreach (Document document in documents)
            {
                batchWrite.AddDocumentToPut(document);

                i++;
                if (i == 25)
                {
                    i = 0;
                    await batchWrite.ExecuteAsync();
                    batchWrite = table.CreateBatchWrite();
                }
            }

            if (i > 0)
            {
                await batchWrite.ExecuteAsync();
            }
        }

        private async Task BatchDelete(IEnumerable<Primitive> entityKeys)
        {
            DocumentBatchWrite batchWrite = table.CreateBatchWrite();
            int i = 0;
            foreach (Primitive keyPrimitive in entityKeys)
            {
                batchWrite.AddKeyToDelete(keyPrimitive);

                i++;
                if (i == 25)
                {
                    i = 0;
                    await batchWrite.ExecuteAsync();
                    batchWrite = table.CreateBatchWrite();
                }
            }

            if (i > 0)
            {
                await batchWrite.ExecuteAsync();
            }
        }

        public async Task<IEnumerable<TEntity>> GetAsync(IEnumerable<object> entityKeys)
        {
            if (!entityKeys.Any())
            {
                return new List<TEntity>();
            }

            var entityKeysToFetch = new List<object>(entityKeys);
            entityKeysToFetch.RemoveAll(key => loadedEntities.Any(e => EntityHasKey(e, key)));
            entityKeysToFetch.RemoveAll(key => addedEntities.Any(e => EntityHasKey(e, key)));

            IList<TEntity> entities = GetMissingEntities(entityKeys, entityKeysToFetch);

            IEnumerable<Document> fetchedDocuments = await BatchGet(entityKeys.Select(k => k.ToPrimitive()));
            IList<TEntity> fetchedEntities = fetchedDocuments.Select(d => classMapper.DocumentToEntity(d)).ToList();

            loadedEntities.AddRange(fetchedEntities);
            originalEntities.AddRange(fetchedEntities.Select(e => e.Clone()));

            return entities.Concat(fetchedEntities);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var entities = new List<TEntity>();
            List<Document> scanResults;
            var filter = new ScanFilter();

            foreach (PropertyInfo propertyInfo in allPropertyInfos)
            {
                string documentKey = mappingResolver.GetDocumentKeyName(propertyInfo.Name);
                filter.AddCondition(documentKey, ScanOperator.IsNotNull);
            }

            Search search = table.Scan(new ScanFilter());

            do
            {
                scanResults = search.GetNextSet();
                search.Matches.Clear();
                foreach (Document document in scanResults)
                {
                    TEntity entity = classMapper.DocumentToEntity(document);
                    entities.Add(entity);
                }
            } while (scanResults.Any());

            for (int i = 0; i < entities.Count; i++)
            {
                object entityKey = hashKeyPropertyInfo.GetValue(entities[i]);
                TEntity entity = loadedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));

                if (entity == null)
                {
                    entity = addedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));
                }

                if (entity != null)
                {
                    entities[i] = entity;
                }
            }

            return entities;
        }

        public bool Contains(object entityKey)
        {
            return loadedEntities.Any(e => EntityHasKey(e, entityKey)) ||
                addedEntities.Any(e => EntityHasKey(e, entityKey));
        }

        public bool Contains(TEntity entity)
        {
            return loadedEntities.Contains(entity) || addedEntities.Contains(entity);
        }

        private async Task ProcessDeletedEntities()
        {
            if (!deletedEntities.Any())
            {
                return;
            }

            foreach (TEntity entity in deletedEntities)
            {
                loadedEntities.Remove(entity);
            }

            IEnumerable<Primitive> entityKeys = deletedEntities.Select(e => hashKeyPropertyInfo.GetValue(e).ToPrimitive());
            await BatchDelete(entityKeys);

            deletedEntities.Clear();
        }

        private async Task ProcessAddedEntities()
        {
            if (!addedEntities.Any())
            {
                return;
            }

            IEnumerable<Document> documentsToAdd = addedEntities.Select(e => classMapper.EntityToDocument(e));
            await BatchPut(documentsToAdd);

            loadedEntities.AddRange(addedEntities);
            originalEntities.AddRange(addedEntities.Select(e => e.Clone()));
            addedEntities.Clear();
        }

        private async Task ProcessLoadedEntities()
        {
            if (!loadedEntities.Any())
            {
                return;
            }

            int entitiesCount = loadedEntities.Count;
            for (int i = 0; i < entitiesCount; i++)
            {
                TEntity loadedEntity = loadedEntities[i];
                TEntity originalEntity = originalEntities[i];

                foreach (PropertyInfo propertyInfo in allPropertyInfos)
                {
                    object fetchedValue = propertyInfo.GetValue(originalEntity);
                    object loadedValue = propertyInfo.GetValue(loadedEntity);
                    if (!Equals(fetchedValue, loadedValue))
                    {
                        Document documentToUpdate = classMapper.EntityToDocument(loadedEntity);
                        await table.UpdateItemAsync(documentToUpdate);
                        break;
                    }
                }
            }
        }

        public async Task SaveChangesAsync()
        {
            await ProcessDeletedEntities();
            await ProcessLoadedEntities();
            await ProcessAddedEntities();
        }
    }
}
