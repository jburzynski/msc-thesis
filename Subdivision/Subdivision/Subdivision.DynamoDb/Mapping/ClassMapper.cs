﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Amazon.DynamoDBv2.DocumentModel;

namespace Subdivision.DynamoDb.Mapping
{
    public class ClassMapper<TEntity> where TEntity : class
    {
        private IDictionary<string, PropertyInfo> documentFieldsMapping;

        public ClassMapper(MappingResolver mappingResolver)
        {
            documentFieldsMapping = new Dictionary<string, PropertyInfo>();

            Type entityType = typeof(TEntity);
            IEnumerable<PropertyInfo> entityPropertyInfos = mappingResolver.GetEntityPropertyInfos(entityType);

            foreach (PropertyInfo entityPropertyInfo in entityPropertyInfos)
            {
                string documentKey = mappingResolver.GetDocumentKeyName(entityPropertyInfo.Name);
                documentFieldsMapping.Add(documentKey, entityPropertyInfo);
            }
        }

        public TEntity DocumentToEntity(Document document)
        {
            if (document == null)
            {
                return null;
            }

            Type entityType = typeof(TEntity);
            TEntity result = (TEntity)Activator.CreateInstance(entityType);

            foreach (KeyValuePair<string, PropertyInfo> fieldMapping in documentFieldsMapping)
            {
                PropertyInfo fieldPropertyInfo = fieldMapping.Value;
                string documentKey = fieldMapping.Key;
                Primitive fieldValue = (Primitive)document[documentKey];

                object castFieldValue = fieldValue.ToObject(fieldPropertyInfo.PropertyType);

                fieldPropertyInfo.SetValue(result, castFieldValue);
            }

            return result;
        }

        public Document EntityToDocument(TEntity entity)
        {
            if (entity == null)
            {
                return null;
            }

            Document result = new Document();

            foreach (KeyValuePair<string, PropertyInfo> fieldMapping in documentFieldsMapping)
            {
                PropertyInfo fieldPropertyInfo = fieldMapping.Value;
                string documentKey = fieldMapping.Key;
                object fieldValue = fieldPropertyInfo.GetValue(entity);
                Primitive fieldPrimitive = fieldValue.ToPrimitive();
                result[documentKey] = fieldPrimitive;
            }

            return result;
        }
    }
}
