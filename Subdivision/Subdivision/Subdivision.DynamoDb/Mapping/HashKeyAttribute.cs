﻿using System;

namespace Subdivision.DynamoDb.Mapping
{
    [AttributeUsage(AttributeTargets.Property)]
    public class HashKeyAttribute : Attribute
    {
    }
}
