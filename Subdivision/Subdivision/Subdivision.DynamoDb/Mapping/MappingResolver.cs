﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Subdivision.Core;

namespace Subdivision.DynamoDb.Mapping
{
    public class MappingResolver
    {
        public string GetDocumentKeyName(string propertyName)
        {
            IEnumerable<string> chars = propertyName.Select((c, i) => i > 0 && char.IsUpper(c) ? "_" + c.ToString() : c.ToString());
            return string.Concat(chars).ToLowerInvariant();
        }

        public string GetTableName(Type entityType)
        {
            return GetTableNameRecursive(entityType) ?? entityType.Name.ToLowerInvariant();
        }

        private string GetTableNameRecursive(Type entityType)
        {
            TableAttribute tableAttribute = entityType.GetCustomAttribute<TableAttribute>();
            if (tableAttribute != null)
            {
                return tableAttribute.Name;
            }

            Type baseType = entityType.BaseType;
            if (baseType != typeof (object))
            {
                return GetTableNameRecursive(baseType);
            }

            return null;
        }

        public PropertyInfo GetHashKeyPropertyInfo(Type entityType)
        {
            PropertyInfo[] propertyInfos = entityType.GetProperties();
            PropertyInfo hashKeyPropertyInfo = propertyInfos.SingleOrDefault(pi => pi.GetCustomAttribute<HashKeyAttribute>() != null);

            if (hashKeyPropertyInfo != null)
            {
                return hashKeyPropertyInfo;
            }

            Type baseType = entityType.BaseType;
            if (baseType != typeof (object))
            {
                return GetHashKeyPropertyInfo(baseType);
            }

            return null;
        }

        public IEnumerable<PropertyInfo> GetEntityPropertyInfos(Type entityType)
        {
            var result = new List<PropertyInfo>();
            foreach (PropertyInfo propertyInfo in entityType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.SetProperty))
            {
                IgnoreAttribute ignoreAttribute = propertyInfo.GetCustomAttribute<IgnoreAttribute>();
                if (ignoreAttribute == null)
                {
                    result.Add(propertyInfo);
                }
            }
            return result;
        }
    }
}
