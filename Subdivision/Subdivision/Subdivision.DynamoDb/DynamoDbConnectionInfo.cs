﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subdivision.DynamoDb
{
    public class DynamoDbConnectionInfo
    {
        public ConnectionProtocol Protocol { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }

        public string Url
        {
            get
            {
                string protocol = Protocol == ConnectionProtocol.Https ? "https" : "http";
                return string.Format("{0}://{1}:{2}", protocol, Host, Port);
            }
        }
    }
}
