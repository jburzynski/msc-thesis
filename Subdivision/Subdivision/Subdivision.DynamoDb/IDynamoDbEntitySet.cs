﻿using System.Threading.Tasks;

namespace Subdivision.DynamoDb
{
    internal interface IDynamoDbEntitySet
    {
        Task SaveChangesAsync();
    }
}