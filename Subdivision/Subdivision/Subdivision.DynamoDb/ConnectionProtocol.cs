﻿namespace Subdivision.DynamoDb
{
    public enum ConnectionProtocol
    {
        Http, Https
    }
}