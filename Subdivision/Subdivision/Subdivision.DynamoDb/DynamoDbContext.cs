﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Subdivision.Core;

namespace Subdivision.DynamoDb
{
    public abstract class DynamoDbContext : IContext
    {
        public DynamoDbConnectionInfo ConnectionInfo { get; set; }
        protected AmazonDynamoDBClient client;
        protected IEnumerable<PropertyInfo> entitySetPropertyInfos;

        protected DynamoDbContext(DynamoDbConnectionInfo connectionInfo)
        {
            ConnectionInfo = connectionInfo;
            Initialize();
        }

        private void Initialize()
        {
            client = CreateDynamoDbClient();

            entitySetPropertyInfos = GetEntitySetPropertyInfos();
            foreach (PropertyInfo entitySetPropertyInfo in entitySetPropertyInfos)
            {
                CreateEntitySet(entitySetPropertyInfo);
            }
        }

        private AmazonDynamoDBClient CreateDynamoDbClient()
        {
            var config = new AmazonDynamoDBConfig { ServiceURL = ConnectionInfo.Url };
            var credentials = new BasicAWSCredentials(ConnectionInfo.AccessKey, ConnectionInfo.SecretKey);
            return new AmazonDynamoDBClient(credentials, config);
        }

        private IEnumerable<PropertyInfo> GetEntitySetPropertyInfos()
        {
            return GetType().GetProperties()
                .Where(pi => pi.PropertyType.IsGenericType
                             && pi.PropertyType.GetGenericTypeDefinition() == typeof(IEntitySet<>));
        }

        private void CreateEntitySet(PropertyInfo entitySetPropertyInfo)
        {
            Type entityType = entitySetPropertyInfo.PropertyType.GetGenericArguments()[0];
            Type entitySetType = typeof(EntitySet<>).MakeGenericType(entityType);
            object entitySet = Activator.CreateInstance(entitySetType, client);

            entitySetPropertyInfo.SetValue(this, entitySet);
        }

        public void SaveChanges()
        {
            SaveChangesAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public async Task SaveChangesAsync()
        {
            foreach (PropertyInfo propertyInfo in entitySetPropertyInfos)
            {
                IDynamoDbEntitySet entitySet = (IDynamoDbEntitySet)propertyInfo.GetValue(this);
                await entitySet.SaveChangesAsync();
            }
        }

        public void Dispose()
        {
        }
    }
}
