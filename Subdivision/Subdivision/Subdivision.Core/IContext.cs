﻿using System;
using System.Threading.Tasks;

namespace Subdivision.Core
{
    public interface IContext : IDisposable
    {
        void SaveChanges();

        Task SaveChangesAsync();
    }
}