﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Subdivision.Core
{
    public interface IEntitySet
    {
    }

    public interface IEntitySet<TEntity> : IEntitySet where TEntity : class
    {
        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Attach(TEntity entity);

        void Detach(TEntity entity);

        void Delete(TEntity entity);

        void DeleteRange(IEnumerable<TEntity> entities);

        TEntity Get(object entityKey);

        IEnumerable<TEntity> Get(IEnumerable<object> entityKeys);

        IEnumerable<TEntity> GetAll();

        Task<TEntity> GetAsync(object entityKey);

        Task<IEnumerable<TEntity>> GetAsync(IEnumerable<object> entityKeys);

        Task<IEnumerable<TEntity>> GetAllAsync();

        bool Contains(object entityKey);

        bool Contains(TEntity entity);
    }
}