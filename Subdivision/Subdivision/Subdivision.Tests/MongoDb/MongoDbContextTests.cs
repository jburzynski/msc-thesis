﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using Subdivision.Core;
using Subdivision.MongoDb;
using Subdivision.MongoDb.Mapping;

namespace Subdivision.Tests.MongoDb
{
    [TestFixture]
    public class MongoDbContextTests
    {
        private TestContext context;
        private IList<Movie> testMovies; 

        private MongoDbConnectionInfo connectionInfo;
        private IMongoCollection<Movie> mongoCollection;

        public MongoDbContextTests()
        {
            connectionInfo = new MongoDbConnectionInfo
            {
                Host = "localhost",
                Port = 27017,
                Database = "test"
            };

            testMovies = new List<Movie>
            {
                new Movie { Id = "54e35a4e78b9c83c665fd8e7", Name = "Terminator", Duration = 120 },
                new Movie { Id = "54e35a4e78b9c83c665fd8e6", Name = "Aliens", Duration = 100 },
                new Movie { Id = "54e35a4e78b9c83c665fd8e5", Name = "Taxi 19", Duration = 90 }
            };

        }

        [SetUp]
        public void SetUp()
        {
            context = new TestContext(connectionInfo);

            var mongoClient = new MongoClient(connectionInfo.ConnectionString);
            IMongoDatabase mongoDatabase = mongoClient.GetDatabase("test");
            mongoCollection = mongoDatabase.GetCollection<Movie>("test.tutorial.movies");

            mongoCollection.DeleteManyAsync(new BsonDocument()).ConfigureAwait(false).GetAwaiter().GetResult();
            mongoCollection.InsertManyAsync(testMovies).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        [Test]
        public void When_AddedEntity_Then_EntityShouldBePersistedInDb()
        {
            // given

            const string movieTitle = "A sample movie";
            const string movieId = "54e35a4e78b9c83c665fd8e4";
            var newMovie = new Movie
            {
                Id = movieId,
                Name = movieTitle,
                Duration = 100
            };

            // when

            context.Movies.Add(newMovie);
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movieId);

            Assert.IsNotNull(fetchedMovie);
            Assert.AreEqual(newMovie.Id, fetchedMovie.Id);
            Assert.AreEqual(newMovie.Name, fetchedMovie.Name);
            Assert.AreEqual(newMovie.Duration, fetchedMovie.Duration);
        }

        [Test]
        public void When_AddedEntity_Then_EntityShouldBeGettableById()
        {
            // given

            const string movieTitle = "A sample movie";
            var newMovie = new Movie { Name = movieTitle };

            // when

            context.Movies.Add(newMovie);
            context.SaveChanges();

            // then

            Movie movie = context.Movies.Get(newMovie.Id);
            Assert.AreEqual(movieTitle, movie.Name);
        }

        [Test]
        public void When_AddingLoadedEntity_Then_ExceptionShouldBeThrown()
        {
            // given

            Movie movie = context.Movies.Get(testMovies[0].Id);

            // then

            Assert.Catch<ContextException>(() => { context.Movies.Add(movie); });
        }

        [Test]
        public void When_AddedManyEntities_Then_EntitiesShouldBePersistedInDb()
        {
            // given

            const string movie1Title = "A sample movie 1";
            const string movie2Title = "A sample movie 2";
            const string movie1Id = "54e35a4e78b9c83c665fd8e4";
            const string movie2Id = "54e35a4e78b9c83c665fd8e3";

            List<Movie> moviesToAdd = new List<Movie>
            {
                new Movie { Id = movie1Id, Name = movie1Title },
                new Movie { Id = movie2Id, Name = movie2Title }
            };

            // when

            context.Movies.AddRange(moviesToAdd);
            context.SaveChanges();

            // then

            Movie fetchedMovie1 = GetMovie(movie1Id);
            Movie fetchedMovie2 = GetMovie(movie2Id);

            Assert.IsNotNull(fetchedMovie1);
            Assert.IsNotNull(fetchedMovie2);
            Assert.AreEqual(movie1Title, fetchedMovie1.Name);
            Assert.AreEqual(movie2Title, fetchedMovie2.Name);
        }

        [Test]
        public void When_DeletedEntity_Then_EntityShouldBeDeletedFromDb()
        {
            // given

            Movie movie = testMovies[0];

            // when

            context.Movies.Delete(movie);
            context.SaveChanges();

            // then

            Assert.True(IsDeleted(movie.Id));
        }

        [Test]
        public void When_GettingEntity_Then_EntityShouldBeFetched()
        {
            // when

            Movie movie = context.Movies.Get(testMovies[0].Id);

            // then

            Assert.NotNull(movie);
            Assert.AreEqual(testMovies[0].Name, movie.Name);
        }

        [Test]
        public void When_GettingManyEntities_Then_EntitiesShouldBeFetched()
        {
            // when

            IEnumerable<Movie> movies = context.Movies.Get(new[] { testMovies[0].Id, testMovies[1].Id });

            // then

            Assert.NotNull(movies);
            Assert.AreEqual(2, movies.Count());
            Assert.True(movies.All(m =>
                (m.Id == testMovies[0].Id && m.Name == testMovies[0].Name) ||
                (m.Id == testMovies[1].Id && m.Name == testMovies[1].Name)
            ));
        }

        [Test]
        public void When_GettingAllEntities_Then_EntitiesShouldBeFetched()
        {
            // when

            IEnumerable<Movie> movies = context.Movies.GetAll();

            // then

            Assert.NotNull(movies);
            Assert.AreEqual(3, movies.Count());
            Assert.True(movies.All(m =>
                (m.Id == testMovies[0].Id && m.Name == testMovies[0].Name) ||
                (m.Id == testMovies[1].Id && m.Name == testMovies[1].Name) ||
                (m.Id == testMovies[2].Id && m.Name == testMovies[2].Name)
            ));
        }

        [Test]
        public void When_GettingEntityTwice_Then_SameObjectShouldBeReturned()
        {
            // when

            Movie movie1 = context.Movies.Get(testMovies[0].Id);
            Movie movie2 = context.Movies.Get(testMovies[0].Id);

            // then

            Assert.AreSame(movie1, movie2);
        }

        [Test]
        public void When_UpdatingFetchedEntity_Then_EntityShouldBeUpdatedInDb()
        {
            // given

            Movie movie = context.Movies.Get(testMovies[0].Id);
            int newDuration = movie.Duration + 10;

            // when

            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_AttachedThenUpdatedEntity_Then_EntityShouldBeUpdatedInDb()
        {
            // given

            Movie movie = testMovies[0];
            int newDuration = movie.Duration + 10;

            // when

            context.Movies.Attach(movie);
            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_DetachedThenUpdatedEntity_Then_EntityShouldNotBeUpdatedInDb()
        {
            // given

            Movie movie = context.Movies.Get(testMovies[0].Id);
            int newDuration = movie.Duration + 10;

            // when

            context.Movies.Detach(movie);
            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreNotEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_FetchedEntity_Then_ContainsKeyCheckShouldReturnTrue()
        {
            // when

            Movie movie = context.Movies.Get(testMovies[0].Id);

            // then

            Assert.IsTrue(context.Movies.Contains(movie.Id));
        }

        [Test]
        public void When_FetchedEntity_Then_ContainsObjectCheckShouldReturnTrue()
        {
            // when

            Movie movie = context.Movies.Get(testMovies[0].Id);

            // then

            Assert.IsTrue(context.Movies.Contains(movie));
        }

        private bool IsDeleted(string entityKey)
        {
            return GetMovie(entityKey) == null;
        }

        private Movie GetMovie(string entityKey)
        {
            var filter = new BsonDocument(new Dictionary<string, object> { { "_id", new ObjectId(entityKey) } });
            return mongoCollection.Find(filter).SingleOrDefaultAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private class TestContext : MongoDbContext
        {
            public TestContext(MongoDbConnectionInfo connectionInfo)
                : base(connectionInfo)
            {
            }

            public IEntitySet<Movie> Movies { get; set; } 
        }

        [Collection("test.tutorial.movies")]
        private class Movie
        {
            [Id]
            public string Id { get; set; }

            public string Name { get; set; }

            public int Duration { get; set; }
        }
    }

}
