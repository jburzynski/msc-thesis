﻿using System.Collections.Generic;
using System.Linq;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using NUnit.Framework;
using Subdivision.Core;
using Subdivision.DynamoDb;
using Subdivision.DynamoDb.Mapping;

namespace Subdivision.Tests.DynamoDb
{
    [TestFixture]
    public class DynamoDbContextTests
    {
        private TestContext context;
        private IList<Movie> testMovieEntities;
        private IList<Document> testMovieDocuments;

        private DynamoDbConnectionInfo connectionInfo;
        private Table moviesTable;
        private ClassMapper<Movie> classMapper;

        public DynamoDbContextTests()
        {
            connectionInfo = new DynamoDbConnectionInfo
            {
                Host = "localhost",
                Protocol = ConnectionProtocol.Http,
                AccessKey = "dummy",
                SecretKey = "dummy",
                Port = 13579
            };

            testMovieEntities = new List<Movie>
            {
                new Movie { Id = 1, Name = "Terminator", Duration = 120 },
                new Movie { Id = 2, Name = "Aliens", Duration = 100 },
                new Movie { Id = 3, Name = "Taxi 19", Duration = 90 }
            };

            testMovieDocuments = new List<Document>
            {
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", 1}, {"name", "Terminator"}, {"duration", 120} }),
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", 2}, {"name", "Aliens"}, {"duration", 100} }),
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", 3}, {"name", "Taxi 19"}, {"duration", 90} })
            };
        }

        [SetUp]
        public void SetUp()
        {
            var mappingResolver = new MappingResolver();
            classMapper = new ClassMapper<Movie>(mappingResolver);
            AmazonDynamoDBClient client = CreateDynamoDbClient();
            string tableName = mappingResolver.GetTableName(typeof(Movie));
            moviesTable = GetMoviesTable(client, tableName);

            RemoveAllItems();
            foreach (Document document in testMovieDocuments)
            {
                moviesTable.PutItem(document);
            }

            context = new TestContext(connectionInfo);
        }

        private Table GetMoviesTable(AmazonDynamoDBClient client, string tableName)
        {
            try
            {
                return Table.LoadTable(client, tableName);
            }
            catch (ResourceNotFoundException)
            {
                CreateMoviesTable(client, tableName);
                return Table.LoadTable(client, tableName);
            }
        }

        private void CreateMoviesTable(AmazonDynamoDBClient client, string tableName)
        {
            var keyDefinition = new List<KeySchemaElement>
            {
                new KeySchemaElement("id", KeyType.HASH)
            };

            var attributeDefinition = new List<AttributeDefinition>
            {
                new AttributeDefinition("id", ScalarAttributeType.N)
            };

            var request = new CreateTableRequest(tableName, keyDefinition, attributeDefinition, new ProvisionedThroughput(1, 1));
            client.CreateTable(request);
        }

        private void RemoveAllItems()
        {
            List<Document> results;
            Search search = moviesTable.Scan(new ScanFilter());

            do
            {
                results = search.GetNextSet();
                search.Matches.Clear();
                foreach (Document document in results)
                {
                    moviesTable.DeleteItem(document);
                }
            } while (results.Any());
        }

        private AmazonDynamoDBClient CreateDynamoDbClient()
        {
            var config = new AmazonDynamoDBConfig { ServiceURL = connectionInfo.Url };
            var credentials = new BasicAWSCredentials(connectionInfo.AccessKey, connectionInfo.SecretKey);
            return new AmazonDynamoDBClient(credentials, config);
        }

        [Test]
        public void When_AddedEntity_Then_EntityShouldBePersistedInDb()
        {
            // given

            const string movieTitle = "A sample movie";
            const int movieId = 13;
            var newMovie = new Movie
            {
                Id = movieId,
                Name = movieTitle,
                Duration = 100
            };

            // when

            context.Movies.Add(newMovie);
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movieId);

            Assert.IsNotNull(fetchedMovie);
            Assert.AreEqual(newMovie.Id, fetchedMovie.Id);
            Assert.AreEqual(newMovie.Name, fetchedMovie.Name);
            Assert.AreEqual(newMovie.Duration, fetchedMovie.Duration);
        }

        [Test]
        public void When_AddedEntity_Then_EntityShouldBeGettableById()
        {
            // given

            const string movieTitle = "A sample movie";
            var newMovie = new Movie { Name = movieTitle };

            // when

            context.Movies.Add(newMovie);
            context.SaveChanges();

            // then

            Movie movie = context.Movies.Get(newMovie.Id);
            Assert.AreEqual(movieTitle, movie.Name);
        }

        [Test]
        public void When_AddingLoadedEntity_Then_ExceptionShouldBeThrown()
        {
            // given

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);

            // then

            Assert.Catch<ContextException>(() => { context.Movies.Add(movie); });
        }

        [Test]
        public void When_AddedManyEntities_Then_EntitiesShouldBePersistedInDb()
        {
            // given

            const string movie1Title = "A sample movie 1";
            const string movie2Title = "A sample movie 2";
            const int movie1Id = 13;
            const int movie2Id = 31;

            List<Movie> moviesToAdd = new List<Movie>
            {
                new Movie { Id = movie1Id, Name = movie1Title },
                new Movie { Id = movie2Id, Name = movie2Title }
            };

            // when

            context.Movies.AddRange(moviesToAdd);
            context.SaveChanges();

            // then

            Movie fetchedMovie1 = GetMovie(movie1Id);
            Movie fetchedMovie2 = GetMovie(movie2Id);

            Assert.IsNotNull(fetchedMovie1);
            Assert.IsNotNull(fetchedMovie2);
            Assert.AreEqual(movie1Title, fetchedMovie1.Name);
            Assert.AreEqual(movie2Title, fetchedMovie2.Name);
        }

        [Test]
        public void When_DeletedEntity_Then_EntityShouldBeDeletedFromDb()
        {
            // given

            Movie movie = testMovieEntities[0];

            // when

            context.Movies.Delete(movie);
            context.SaveChanges();

            // then

            Assert.True(IsDeleted(movie.Id));
        }

        [Test]
        public void When_GettingEntity_Then_EntityShouldBeFetched()
        {
            // when

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);

            // then

            Assert.NotNull(movie);
            Assert.AreEqual(testMovieEntities[0].Name, movie.Name);
        }

        [Test]
        public void When_GettingManyEntities_Then_EntitiesShouldBeFetched()
        {
            // when

            IEnumerable<Movie> movies = context.Movies.Get(new object[] { testMovieEntities[0].Id, testMovieEntities[1].Id });

            // then

            Assert.NotNull(movies);
            Assert.AreEqual(2, movies.Count());
            Assert.True(movies.All(m =>
                (m.Id == testMovieEntities[0].Id && m.Name == testMovieEntities[0].Name) ||
                (m.Id == testMovieEntities[1].Id && m.Name == testMovieEntities[1].Name)
            ));
        }

        [Test]
        public void When_GettingAllEntities_Then_EntitiesShouldBeFetched()
        {
            // when

            IEnumerable<Movie> movies = context.Movies.GetAll();

            // then

            Assert.NotNull(movies);
            Assert.AreEqual(3, movies.Count());
            Assert.True(movies.All(m =>
                (m.Id == testMovieEntities[0].Id && m.Name == testMovieEntities[0].Name) ||
                (m.Id == testMovieEntities[1].Id && m.Name == testMovieEntities[1].Name) ||
                (m.Id == testMovieEntities[2].Id && m.Name == testMovieEntities[2].Name)
            ));
        }

        [Test]
        public void When_GettingEntityTwice_Then_SameObjectShouldBeReturned()
        {
            // when

            Movie movie1 = context.Movies.Get(testMovieEntities[0].Id);
            Movie movie2 = context.Movies.Get(testMovieEntities[0].Id);

            // then

            Assert.AreSame(movie1, movie2);
        }

        [Test]
        public void When_UpdatingFetchedEntity_Then_EntityShouldBeUpdatedInDb()
        {
            // given

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);
            int newDuration = movie.Duration + 10;

            // when

            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_AttachedThenUpdatedEntity_Then_EntityShouldBeUpdatedInDb()
        {
            // given

            Movie movie = testMovieEntities[0];
            int newDuration = movie.Duration + 10;

            // when

            context.Movies.Attach(movie);
            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_DetachedThenUpdatedEntity_Then_EntityShouldNotBeUpdatedInDb()
        {
            // given

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);
            int newDuration = movie.Duration + 10;

            // when

            context.Movies.Detach(movie);
            movie.Duration = newDuration;
            context.SaveChanges();

            // then

            Movie fetchedMovie = GetMovie(movie.Id);
            Assert.AreNotEqual(newDuration, fetchedMovie.Duration);
        }

        [Test]
        public void When_FetchedEntity_Then_ContainsKeyCheckShouldReturnTrue()
        {
            // when

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);

            // then

            Assert.IsTrue(context.Movies.Contains(movie.Id));
        }

        [Test]
        public void When_FetchedEntity_Then_ContainsObjectCheckShouldReturnTrue()
        {
            // when

            Movie movie = context.Movies.Get(testMovieEntities[0].Id);

            // then

            Assert.IsTrue(context.Movies.Contains(movie));
        }

        private bool IsDeleted(int entityKey)
        {
            return GetMovie(entityKey) == null;
        }

        private Movie GetMovie(int entityKey)
        {
            Document document = moviesTable.GetItem(entityKey);
            return classMapper.DocumentToEntity(document);
        }

        private class TestContext : DynamoDbContext
        {
            public TestContext(DynamoDbConnectionInfo connectionInfo)
                : base(connectionInfo)
            {
            }

            public IEntitySet<Movie> Movies { get; set; }
        }
    }
}
