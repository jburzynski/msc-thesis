﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DocumentModel;
using NUnit.Framework;
using Subdivision.DynamoDb.Mapping;

namespace Subdivision.Tests.DynamoDb
{
    [TestFixture]
    public class ClassMapperTests
    {
        private IList<Movie> testMovieEntities;
        private IList<Document> testMovieDocuments;

        private ClassMapper<Movie> classMapper;

        public ClassMapperTests()
        {
            testMovieEntities = new List<Movie>
            {
                new Movie { Id = 1, Name = "Terminator", Duration = 120 },
                new Movie { Id = 2, Name = "Aliens", Duration = 100 },
                new Movie { Id = 3, Name = "Taxi 19", Duration = 90 }
            };

            testMovieDocuments = new List<Document>
            {
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", (Primitive)1}, {"name", (Primitive)"Terminator"}, {"duration", (Primitive)120} }),
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", (Primitive)2}, {"name", (Primitive)"Aliens"}, {"duration", (Primitive)100} }),
                new Document(new Dictionary<string, DynamoDBEntry> { {"id", (Primitive)3}, {"name", (Primitive)"Taxi 19"}, {"duration", (Primitive)90} })
            };

            classMapper = new ClassMapper<Movie>(new MappingResolver());
        }

        [Test]
        public void When_MappingEntityToDocument_Then_DocumentShouldMatchEntity()
        {
            // given

            var mappedDocuments = new List<Document>();

            // when

            foreach (Movie movie in testMovieEntities)
            {
                Document document = classMapper.EntityToDocument(movie);
                mappedDocuments.Add(document);
            }

            // then

            for (int i = 0; i < mappedDocuments.Count; i++)
            {
                Document expectedDocument = testMovieDocuments[i];
                Document mappedDocument = mappedDocuments[i];

                Assert.AreEqual(expectedDocument["id"], mappedDocument["id"]);
                Assert.IsTrue(expectedDocument["name"].Equals(mappedDocument["name"]));
                Assert.IsTrue(expectedDocument["duration"].Equals(mappedDocument["duration"]));
            }
        }

        [Test]
        public void When_MappingDocumentToEntity_Then_EntityShouldMatchDocument()
        {
            // given

            var mappedEntities = new List<Movie>();

            // when

            foreach (Document document in testMovieDocuments)
            {
                Movie movie = classMapper.DocumentToEntity(document);
                mappedEntities.Add(movie);
            }

            // then

            for (int i = 0; i < mappedEntities.Count; i++)
            {
                Movie expectedMovie = testMovieEntities[i];
                Movie mappedMovie = mappedEntities[i];

                Assert.AreEqual(expectedMovie.Id, mappedMovie.Id);
                Assert.AreEqual(expectedMovie.Name, mappedMovie.Name);
                Assert.AreEqual(expectedMovie.Duration, mappedMovie.Duration);
            }
        }
    }
}
