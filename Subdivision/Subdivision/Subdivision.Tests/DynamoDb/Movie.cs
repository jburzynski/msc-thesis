﻿using Subdivision.DynamoDb.Mapping;

namespace Subdivision.Tests.DynamoDb
{
    [Table("movies")]
    internal class Movie
    {
        [HashKey]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Duration { get; set; }
    }
}
