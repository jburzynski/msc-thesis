﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Driver;
using Subdivision.Core;
using Subdivision.MongoDb.Mapping;

namespace Subdivision.MongoDb
{
    public abstract class MongoDbContext : IContext
    {
        public MongoDbConnectionInfo ConnectionInfo { get; private set; }
        protected MongoClient client;
        protected ClassMapper classMapper;
        protected IEnumerable<PropertyInfo> entitySetPropertyInfos;

        protected MongoDbContext(MongoDbConnectionInfo connectionInfo)
        {
            ConnectionInfo = connectionInfo;
            classMapper = new ClassMapper();

            Initialize();
        }

        private void Initialize()
        {
            client = new MongoClient(ConnectionInfo.ConnectionString);

            entitySetPropertyInfos = GetEntitySetPropertyInfos();
            foreach (PropertyInfo entitySetPropertyInfo in entitySetPropertyInfos)
            {
                Type entityType = entitySetPropertyInfo.PropertyType.GetGenericArguments()[0];
                classMapper.Map(entityType);
                CreateEntitySet(entitySetPropertyInfo);
            }
        }

        private IEnumerable<PropertyInfo> GetEntitySetPropertyInfos()
        {
            return GetType().GetProperties()
                .Where(pi => pi.PropertyType.IsGenericType
                             && pi.PropertyType.GetGenericTypeDefinition() == typeof(IEntitySet<>));
        }

        private void CreateEntitySet(PropertyInfo entitySetPropertyInfo)
        {
            Type entityType = entitySetPropertyInfo.PropertyType.GetGenericArguments()[0];
            Type entitySetType = typeof(EntitySet<>).MakeGenericType(entityType);
            object entitySet = Activator.CreateInstance(entitySetType, client, ConnectionInfo.Database);

            entitySetPropertyInfo.SetValue(this, entitySet);
        }

        public void SaveChanges()
        {
            SaveChangesAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public async Task SaveChangesAsync()
        {
            foreach (PropertyInfo propertyInfo in entitySetPropertyInfos)
            {
                IMongoDbEntitySet entitySet = (IMongoDbEntitySet)propertyInfo.GetValue(this);
                await entitySet.SaveChangesAsync();
            }
        }

        public void Dispose()
        {
        }
    }
}
