﻿using System.Threading.Tasks;

namespace Subdivision.MongoDb
{
    internal interface IMongoDbEntitySet
    {
        Task SaveChangesAsync();
    }
}