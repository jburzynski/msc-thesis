﻿namespace Subdivision.MongoDb.Mapping
{
    public enum IdType
    {
        ObjectId,
        Custom
    }
}