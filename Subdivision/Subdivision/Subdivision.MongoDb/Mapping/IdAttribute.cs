﻿using System;

namespace Subdivision.MongoDb.Mapping
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IdAttribute : Attribute
    {
        public IdType IdType { get; set; }

        public IdAttribute()
        {
            IdType = IdType.ObjectId;
        }

        public IdAttribute(IdType idType)
        {
            IdType = idType;
        }
    }
}
