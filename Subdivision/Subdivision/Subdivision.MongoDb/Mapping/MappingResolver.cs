﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Subdivision.Core;

namespace Subdivision.MongoDb.Mapping
{
    public class MappingResolver
    {
        public string GetCollectionName(Type entityType)
        {
            return GetCollectionNameRecursive(entityType) ?? entityType.Name.ToLowerInvariant();
        }

        private string GetCollectionNameRecursive(Type entityType)
        {
            CollectionAttribute collectionAttribute = entityType.GetCustomAttribute<CollectionAttribute>();
            if (collectionAttribute != null)
            {
                return collectionAttribute.Name;
            }

            Type baseType = entityType.BaseType;
            if (baseType != typeof(object))
            {
                return GetCollectionNameRecursive(baseType);
            }

            return null;
        }

        public PropertyInfo GetIdPropertyInfo(Type entityType)
        {
            PropertyInfo[] propertyInfos = entityType.GetProperties();
            PropertyInfo idPropertyInfo = propertyInfos.SingleOrDefault(pi => pi.GetCustomAttribute<IdAttribute>() != null);

            if (idPropertyInfo != null)
            {
                return idPropertyInfo;
            }

            Type baseType = entityType.BaseType;
            if (baseType != typeof(object))
            {
                return GetIdPropertyInfo(baseType);
            }

            return null;
        }

        public IEnumerable<PropertyInfo> GetEntityPropertyInfos(Type entityType)
        {
            var result = new List<PropertyInfo>();
            foreach (PropertyInfo propertyInfo in entityType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.SetProperty))
            {
                IgnoreAttribute ignoreAttribute = propertyInfo.GetCustomAttribute<IgnoreAttribute>();
                if (ignoreAttribute == null)
                {
                    result.Add(propertyInfo);
                }
            }
            return result;
        }
    }
}
