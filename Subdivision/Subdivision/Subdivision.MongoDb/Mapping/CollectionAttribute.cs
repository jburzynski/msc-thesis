﻿using System;

namespace Subdivision.MongoDb.Mapping
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CollectionAttribute : Attribute
    {
        public string Name { get; set; }

        public CollectionAttribute(string name)
        {
            Name = name;
        }
    }
}
