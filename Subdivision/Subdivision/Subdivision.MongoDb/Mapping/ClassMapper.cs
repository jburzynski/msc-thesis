﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using Subdivision.Core;

namespace Subdivision.MongoDb.Mapping
{
    public class ClassMapper
    {
        public void Map(Type entityType)
        {
            if (BsonClassMap.IsClassMapRegistered(entityType))
            {
                return;
            }

            ValidateType(entityType);

            BsonClassMap classMap = CreateBsonClassMap(entityType);
            foreach (PropertyInfo propertyInfo in entityType.GetProperties())
            {
                MapProperty(propertyInfo, classMap);
            }
            BsonClassMap.RegisterClassMap(classMap);

            Type baseType = entityType.BaseType;
            if (baseType != typeof (object))
            {
                Map(baseType);
            }
        }

        private void MapProperty(PropertyInfo propertyInfo, BsonClassMap classMap)
        {
            if (propertyInfo.GetCustomAttribute<IgnoreAttribute>() != null)
            {
                return;
            }

            IdAttribute idAttribute = propertyInfo.GetCustomAttribute<IdAttribute>();
            if (idAttribute != null)
            {
                MapIdProperty(propertyInfo, classMap);
                return;
            }

            classMap.MapProperty(propertyInfo.Name);
        }

        private void MapIdProperty(PropertyInfo propertyInfo, BsonClassMap classMap)
        {
            IdAttribute idAttribute = propertyInfo.GetCustomAttribute<IdAttribute>();
            classMap.MapIdProperty(propertyInfo.Name);

            if (idAttribute.IdType == IdType.ObjectId && propertyInfo.PropertyType == typeof(string))
            {
                classMap.IdMemberMap.SetSerializer(new StringSerializer(BsonType.ObjectId));
                classMap.IdMemberMap.SetIdGenerator(StringObjectIdGenerator.Instance);
            }
        }

        private BsonClassMap CreateBsonClassMap(Type entityType)
        {
            Type bsonClassMapType = typeof(BsonClassMap<>);
            Type genericBsonClassMapType = bsonClassMapType.MakeGenericType(entityType);
            return (BsonClassMap)Activator.CreateInstance(genericBsonClassMapType);
        }

        private void ValidateType(Type entityType)
        {
            IEnumerable<PropertyInfo> propertyInfos = GetNestedPropertyInfos(entityType);

            if (propertyInfos.All(pi => pi.GetCustomAttribute<IdAttribute>() == null))
            {
                throw new MappingException(String.Format("Id property for entity class '{0}' must be provided (use Subdivision.MongoDb.Mapping.IdAttribute).", entityType.FullName));
            }

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                IdAttribute idAttribute = propertyInfo.GetCustomAttribute<IdAttribute>();
                Type propertyType = propertyInfo.PropertyType;
                if (idAttribute != null && idAttribute.IdType == IdType.ObjectId && propertyType != typeof (ObjectId) && propertyType != typeof (string))
                {
                    throw new MappingException(String.Format("Id attribute of type IdType.ObjectId can only be assigned to a property of type MongoDB.Bson.ObjectId or string (in class '{0}').", entityType.FullName));
                }
            }
        }

        private IEnumerable<PropertyInfo> GetNestedPropertyInfos(Type entityType)
        {
            PropertyInfo[] propertyInfos = entityType.GetProperties();

            Type baseType = entityType.BaseType;
            if (baseType == typeof(object))
            {
                return propertyInfos;
            }

            return propertyInfos.Concat(GetNestedPropertyInfos(baseType));
        }

    }
}
