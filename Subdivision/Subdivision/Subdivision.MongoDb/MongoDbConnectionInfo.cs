﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Subdivision.Core;

namespace Subdivision.MongoDb
{
    public class MongoDbConnectionInfo
    {
        private const string Protocol = "mongodb";
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public IDictionary<string, string> Params { get; set; }

        public MongoDbConnectionInfo()
        {
            Params = new Dictionary<string, string>();
        }

        public string ConnectionString
        {
            get
            {
                string result = Protocol + "://";
                if (!String.IsNullOrEmpty(UserName))
                {
                    result += UserName;
                    if (!String.IsNullOrEmpty(Password))
                    {
                        result += ":" + Password;
                    }
                    result += "@";
                }

                result += Host + ":" + Port;

                if (!String.IsNullOrEmpty(Database) || Params.Any())
                {
                    result += "/" + (Database ?? String.Empty);
                    if (Params.Any())
                    {
                        result += "?" + String.Join(";", Params.Select(kvp => kvp.Key + "=" + kvp.Value));
                    }
                }

                return result;
            }
        }
    }
}
