﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Subdivision.Core;
using Subdivision.MongoDb.Mapping;

namespace Subdivision.MongoDb
{
    internal class EntitySet<TEntity> : IMongoDbEntitySet, IEntitySet<TEntity> where TEntity : class
    {
        private readonly MongoClient mongoClient;
        private readonly MappingResolver mappingResolver;
        private readonly IMongoCollection<TEntity> mongoCollection;

        private readonly List<TEntity> originalEntities;
        private readonly List<TEntity> loadedEntities;
        private readonly List<TEntity> addedEntities;
        private readonly List<TEntity> deletedEntities;

        private readonly PropertyInfo idPropertyInfo;
        private readonly IEnumerable<PropertyInfo> allPropertyInfos;

        public EntitySet(MongoClient client, string databaseName)
        {
            Type entityType = typeof (TEntity);
            mongoClient = client;
            mappingResolver = new MappingResolver();

            idPropertyInfo = mappingResolver.GetIdPropertyInfo(entityType);
            allPropertyInfos = mappingResolver.GetEntityPropertyInfos(entityType);

            originalEntities = new List<TEntity>();
            loadedEntities = new List<TEntity>();
            addedEntities = new List<TEntity>();
            deletedEntities = new List<TEntity>();

            mongoCollection = CreateMongoCollection(databaseName);
        }

        private IMongoCollection<TEntity> CreateMongoCollection(string databaseName)
        {
            Type entityType = typeof(TEntity);
            string collectionName = mappingResolver.GetCollectionName(entityType);
            IMongoDatabase mongoDatabase = mongoClient.GetDatabase(databaseName);
            return mongoDatabase.GetCollection<TEntity>(collectionName);
        }

        private bool EntityHasKey(TEntity entity, object key)
        {
            object entityId = idPropertyInfo.GetValue(entity);
            return entityId != null && entityId.Equals(key);
        }

        public void Add(TEntity entity)
        {
            if (addedEntities.Contains(entity) || loadedEntities.Contains(entity))
            {
                throw new ContextException("The entity was already added or loaded.");
            }
            addedEntities.Add(entity);
            deletedEntities.Remove(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (addedEntities.Any(entities.Contains) || loadedEntities.Any(entities.Contains))
            {
                throw new ContextException("The entity was already added or loaded.");
            }
            addedEntities.AddRange(entities);

            foreach (TEntity entity in entities)
            {
                deletedEntities.Remove(entity);
            }
        }

        public void Attach(TEntity entity)
        {
            if (addedEntities.Contains(entity) || loadedEntities.Contains(entity))
            {
                return;
            }

            if (idPropertyInfo.GetValue(entity) != null)
            {
                loadedEntities.Add(entity);
                originalEntities.Add(entity.Clone());
                return;
            }

            addedEntities.Add(entity);
        }

        public void Detach(TEntity entity)
        {
            addedEntities.Remove(entity);
            loadedEntities.Remove(entity);
            deletedEntities.Remove(entity);
        }

        public void Delete(TEntity entity)
        {
            if (deletedEntities.Contains(entity))
            {
                throw new ContextException("The entity was already deleted.");
            }
            deletedEntities.Add(entity);
            addedEntities.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (deletedEntities.Any(entities.Contains))
            {
                throw new ContextException("The entity was already deleted.");
            }
            deletedEntities.AddRange(entities);

            foreach (TEntity entity in entities)
            {
                addedEntities.Remove(entity);
            }
        }

        public TEntity Get(object entityKey)
        {
            return GetAsync(entityKey).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public IEnumerable<TEntity> Get(IEnumerable<object> entityKeys)
        {
            return GetAsync(entityKeys).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return GetAllAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public async Task<TEntity> GetAsync(object entityKey)
        {
            if (loadedEntities.Any(e => EntityHasKey(e, entityKey)))
            {
                return loadedEntities.First(e => EntityHasKey(e, entityKey));
            }

            if (addedEntities.Any(e => EntityHasKey(e, entityKey)))
            {
                return addedEntities.First(e => EntityHasKey(e, entityKey));
            }

            object queryKey = GetEntityIdForQuery(entityKey);

            var filter = new BsonDocument(new Dictionary<string, object> { { "_id", queryKey } });
            TEntity entity = await mongoCollection.Find(filter).SingleOrDefaultAsync();
            if (entity != null)
            {
                loadedEntities.Add(entity);
                originalEntities.Add(entity.Clone());
            }

            return entity;
        }

        private object GetEntityIdForQuery(object entityKey)
        {
            if (idPropertyInfo.GetCustomAttribute<IdAttribute>().IdType == IdType.ObjectId && entityKey is string)
            {
                return new ObjectId((string)entityKey);
            }
            return entityKey;
        }

        private IList<TEntity> GetMissingEntities(IEnumerable<object> entityKeys, List<object> entityKeysToFetch)
        {
            var entities = new List<TEntity>();

            foreach (object entityKey in entityKeys)
            {
                if (!entityKeysToFetch.Contains(entityKey))
                {
                    TEntity entity = loadedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));

                    if (entity == null)
                    {
                        entity = addedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));
                    }

                    if (entity != null)
                    {
                        entities.Add(entity);
                    }
                }
            }

            return entities;
        }

        public async Task<IEnumerable<TEntity>> GetAsync(IEnumerable<object> entityKeys)
        {
            if (!entityKeys.Any())
            {
                return new List<TEntity>();
            }

            var entityKeysToFetch = new List<object>(entityKeys);
            entityKeysToFetch.RemoveAll(key => loadedEntities.Any(e => EntityHasKey(e, key)));
            entityKeysToFetch.RemoveAll(key => addedEntities.Any(e => EntityHasKey(e, key)));

            IList<TEntity> entities = GetMissingEntities(entityKeys, entityKeysToFetch);

            if (idPropertyInfo.GetCustomAttribute<IdAttribute>().IdType == IdType.ObjectId &&
                entityKeys.First() is string)
            {
                for (int i = 0; i < entityKeysToFetch.Count; i++)
                {
                    entityKeysToFetch[i] = new ObjectId((string)entityKeysToFetch[i]);
                }
            }

            var inDictionary = new Dictionary<string, IEnumerable> { { "$in", entityKeysToFetch } };
            var filter = new BsonDocument(new Dictionary<string, object> { { "_id", inDictionary } });
            IList<TEntity> fetchedEntities = await mongoCollection.Find(filter).ToListAsync();

            loadedEntities.AddRange(fetchedEntities);
            originalEntities.AddRange(fetchedEntities.Select(e => e.Clone()));

            return entities.Concat(fetchedEntities);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var filter = new BsonDocument();
            IList<TEntity> entities = await mongoCollection.Find(filter).ToListAsync();
            for (int i = 0; i < entities.Count; i++)
            {
                object entityKey = idPropertyInfo.GetValue(entities[i]);
                TEntity entity = loadedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));

                if (entity == null)
                {
                    entity = addedEntities.FirstOrDefault(e => EntityHasKey(e, entityKey));
                }

                if (entity != null)
                {
                    entities[i] = entity;
                }
            }

            return entities;
        }

        public bool Contains(object entityKey)
        {
            return loadedEntities.Any(e => EntityHasKey(e, entityKey)) ||
                addedEntities.Any(e => EntityHasKey(e, entityKey));
        }

        public bool Contains(TEntity entity)
        {
            return loadedEntities.Contains(entity) || addedEntities.Contains(entity);
        }

        private async Task ProcessDeletedEntities()
        {
            if (!deletedEntities.Any())
            {
                return;
            }

            foreach (TEntity entity in deletedEntities)
            {
                loadedEntities.Remove(entity);
            }

            IEnumerable<object> deletedEntityKeys = deletedEntities.Select(e => GetEntityIdForQuery(idPropertyInfo.GetValue(e)));
            var inDictionary = new Dictionary<string, IEnumerable> { { "$in", deletedEntityKeys } };
            var filter = new BsonDocument(new Dictionary<string, object> { { "_id", inDictionary } });
            await mongoCollection.DeleteManyAsync(filter);

            deletedEntities.Clear();
        }

        private async Task ProcessAddedEntities()
        {
            if (!addedEntities.Any())
            {
                return;
            }

            await mongoCollection.InsertManyAsync(addedEntities);

            loadedEntities.AddRange(addedEntities);
            originalEntities.AddRange(addedEntities.Select(e => e.Clone()));
            addedEntities.Clear();
        }

        private async Task ProcessLoadedEntities()
        {
            if (!loadedEntities.Any())
            {
                return;
            }

            int entitiesCount = loadedEntities.Count;
            for (int i = 0; i < entitiesCount; i++)
            {
                TEntity loadedEntity = loadedEntities[i];
                TEntity originalEntity = originalEntities[i];

                foreach (PropertyInfo propertyInfo in allPropertyInfos)
                {
                    object fetchedValue = propertyInfo.GetValue(originalEntity);
                    object loadedValue = propertyInfo.GetValue(loadedEntity);
                    if (!Equals(fetchedValue, loadedValue))
                    {
                        object entityKey = idPropertyInfo.GetValue(loadedEntity);
                        var updateFilter = new BsonDocument(new Dictionary<string, object> { { "_id", GetEntityIdForQuery(entityKey) } });
                        await mongoCollection.ReplaceOneAsync(updateFilter, loadedEntity);
                        break;
                    }
                }
            }
        }

        public async Task SaveChangesAsync()
        {
            await ProcessDeletedEntities();
            await ProcessLoadedEntities();
            await ProcessAddedEntities();
        }

    }
}
