Qualitative comparison:

- architecture
- data structures
- environment (programming language etc.)
- capabilities

Methods:
- Subdivision
- PubComp.NoSQL
- DynamoDB SDK (general)
- MongoDB SDK (general)
- DynamoDB REST API
- MongoDB REST API
- DynamoDB Admin UI
- MongoDB Admin UI
- DynamoDB console
- MongoDB console