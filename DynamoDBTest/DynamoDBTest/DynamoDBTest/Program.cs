﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamoDBTest
{
    class Program
    {
        private static readonly int port = 13579;

        static void Main(string[] args)
        {
            // connect

            var config = new AmazonDynamoDBConfig
            {
                ServiceURL = "http://localhost:13579"
            };
            var credentials = new BasicAWSCredentials("dummy", "dummy");
            var client = new AmazonDynamoDBClient(credentials, config);

            // create movies and tickets tables

            var moviesKeyDefinition = new List<KeySchemaElement>
            {
                new KeySchemaElement("mid", KeyType.HASH)
            };

            var moviesAttributeDefinition = new List<AttributeDefinition>
            {
                new AttributeDefinition("mid", ScalarAttributeType.N)
                //new AttributeDefinition("name", ScalarAttributeType.S),
                //new AttributeDefinition("duration", ScalarAttributeType.N)
            };

            var ticketsKeyDefinition = new List<KeySchemaElement>
            {
                new KeySchemaElement("tid", KeyType.HASH),      // mid:date
                new KeySchemaElement("place", KeyType.RANGE)
            };

            var ticketsAttributeDefinition = new List<AttributeDefinition>
            {
                new AttributeDefinition("tid", ScalarAttributeType.S),
                new AttributeDefinition("place", ScalarAttributeType.S)
                //new AttributeDefinition("type", ScalarAttributeType.S)
            };

            // conditional put
            // eventually / strongly consitent reads
            // lsi - a second range key / gsi - another primary key
            // putItem - normal or document model

            var moviesTableRequest = new CreateTableRequest("movies", moviesKeyDefinition, moviesAttributeDefinition, new ProvisionedThroughput(1, 1));
            var ticketsTableRequest = new CreateTableRequest("tickets", ticketsKeyDefinition, ticketsAttributeDefinition, new ProvisionedThroughput(1, 1));

            ListTablesResponse tablesResponse = client.ListTables();
            if (!tablesResponse.TableNames.Contains("movies"))
            {
                client.CreateTable(moviesTableRequest);
            }

            if (!tablesResponse.TableNames.Contains("tickets"))
            {
                client.CreateTable(ticketsTableRequest);
            }

            // list tables

            Console.WriteLine("Tables:");

            foreach (string table in tablesResponse.TableNames)
            {
                Console.WriteLine(table);
            }

            Console.ReadLine();

            // insert data

            //var putMovieRequest = new PutItemRequest("movies",
            //    new Dictionary<string, AttributeValue> {
                    //{ "mid", new AttributeValue("1") }
                    //{ "name", new AttributeValue("Terminator") },
                    //{ "duration", new AttributeValue("120") }
                //})
            //{
            //    ConditionExpression = "attribute_not_exists(mid)"
            //}
            //;
            //client.PutItem(putMovieRequest);
            Table moviesTable = Table.LoadTable(client, "movies");

            var movie = new Document();
            movie["mid"] = 1;
            //movie["name"] = "Terminator";

            moviesTable.PutItem(movie);

            DynamoDBContext context = new DynamoDBContext(client);


            //client.PutItem("movies", new Dictionary<string, AttributeValue> { { "mid", new } });

        }
    }
}
